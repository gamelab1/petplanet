﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GeneratorManager : MonoBehaviour
{
    private static GeneratorManager instance = null;
    
    [Serializable]
    public enum TypesEnum
    {
        Asteroid,
        Planet,
        Trash
    }

    [Serializable]
    public struct TypedPrefab {
        public TypesEnum type;
        public GameObject prefab;
    }

    public TypedPrefab[] Prefabs;

    private Dictionary<TypesEnum, Type> _generatorTypes = new Dictionary<TypesEnum, Type>()
    {
        {TypesEnum.Asteroid, typeof(AsteroidsGenerator)},
        {TypesEnum.Trash, typeof(TrashGenerator)}
    };

    private Dictionary<Type, string> _typesTag = new Dictionary<Type, string>()
    {
        {typeof(AsteroidsGenerator), "AsteroidsPool"},
        {typeof(TrashGenerator), "TrashPool"},
    };

    public CollisionHandler<AsteroidFragment> collisionHandler = new CollisionHandler<AsteroidFragment>((gM, list) =>
    {
        if (list.Count == 2)
        {
            AsteroidCollision asteroidCollision = new AsteroidCollision(list[0].Item1, list[1].Item1);
            return gM._asteroidCollisions.Add(asteroidCollision)
                ? Enumerable.Concat(list[0].Item2, list[1].Item2)
                : new List<AsteroidFragment>();
        }

        return new List<AsteroidFragment>();
    });

    private Dictionary<Type, Type> _dictionaryOfCast = new Dictionary<Type, Type>
    {
        {typeof(AsteroidsGenerator), typeof(IEnumerable<AsteroidFragment>)}
    };

    private Generator[] _generators;

    private Dictionary<Type, Generator> _generatorsByType = new Dictionary<Type, Generator>();

    private Dictionary<Type, float> _generatorTickDictionary = new Dictionary<Type, float>()
    {
        {typeof(AsteroidsGenerator), 5f},
        {typeof(TrashGenerator), 5f}
    };

    private Dictionary<Type, Coroutine> _generatorCoroutinesDictionary = new Dictionary<Type, Coroutine>();
    private Dictionary<Type, Coroutine> _burstCoroutinesDictionary = new Dictionary<Type, Coroutine>();

    private HashSet<AsteroidCollision> _asteroidCollisions = new HashSet<AsteroidCollision>(new AsteroidCollision.AsteroidCollisionEqualityComparer());

    public static Dictionary<GeneratorClass, Type> _generatorClass = new Dictionary<GeneratorClass, Type>()
    {
        {GeneratorClass.Asteroid, typeof(AsteroidsGenerator)},
        {GeneratorClass.Trash, typeof(TrashGenerator)}
    };

    static GeneratorManager()
    {
    }

    private GeneratorManager()
    {
    }

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

        var prefabGeneratorDict = Prefabs.ToDictionary(k => _generatorTypes[k.type], v => v.prefab);
         
        _generators = _generatorTickDictionary.Keys.Select(k => (Generator) Activator.CreateInstance(
            k, 
            new object[]{prefabGeneratorDict[k], _typesTag[k]})
        ).ToArray();

        _generatorsByType = _generators.ToDictionary(k => k.GetType(), e => e);

        _generatorCoroutinesDictionary = _generators
            .ToDictionary(
                g => g.GetType(),
                gg => gg.generate(this, _generatorTickDictionary[gg.GetType()])
            );
    }

    public void handleCollision<T>(
        List<(Type, GameObject, IEnumerable<T>)> collisionList)
    {
        
        //Handle double triggering with the hashset

        if (collisionList.Count == 2)
        {
            AsteroidCollision asteroidCollision = new AsteroidCollision(collisionList[0].Item2, collisionList[1].Item2);

            if (this._asteroidCollisions.Add(asteroidCollision))
            {
                collisionList.ForEach(collision =>
                {
                    triggerGenerationBurst(collision.Item1, collision.Item3);
                });
            }
        }
    }
    
    public void triggerGenerationBurst<T>(Type type, IEnumerable<T> properties)
    {
        if (_generatorsByType.ContainsKey(type))
        {
            StartCoroutine(_generatorsByType[type].burst<T>(properties));
        }
    }

    public static GeneratorManager Instance
    {
        get { return instance; }
    }
}
