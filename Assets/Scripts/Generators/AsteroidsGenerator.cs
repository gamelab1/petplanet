﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;

public class AsteroidsGenerator : Generator
{
    private float radius = 10f;
    
    public AsteroidsGenerator(GameObject g, string tag) : base(g)
    {
        var gg = GameObject.FindGameObjectWithTag(tag);
        Parent = gg != null ? gg.transform : null;
        MAXNumElement = 10;
        GeneratorStep = 3;
    }

    public override IEnumerator generation(float tick)
    {
        //first generation
#if UNITY_EDITOR        
        //Debug.Log($"{this.GetType()} generate stuff for the first time");
#endif        

        Enumerable.Range(1, getTargetNumOfNextGeneration())
            .ToList()
            .ForEach(i =>
            {
                var asteroid = GameObject.Instantiate(Prefab, Parent);
                asteroid.transform.position = (Vector2) GameObject.FindGameObjectWithTag("Player").transform.position
                                              + Utils.getSpawnPosition(radius);
            });

        while (true)
        {
            yield return new WaitForSeconds(tick);
            
#if UNITY_EDITOR            
            Debug.Log($"{this.GetType()} generate stuff");
#endif            
            
            Enumerable.Range(1, getTargetNumOfNextGeneration())
                .ToList()
                .ForEach(i =>
                {
                    var asteroid = GameObject.Instantiate(Prefab, Parent);
                    asteroid.transform.position = (Vector2) GameObject.FindGameObjectWithTag("Player").transform.position
                                                  + Utils.getSpawnPosition(radius);
                });
        }
    }

    public override IEnumerator burst<T>(IEnumerable<T> enumerable)
    {
        enumerable
            .Where(p => p is AsteroidFragment)
            .OfType<AsteroidFragment>()
            .ToList()
            .ForEach(f =>
            {
                var asteroid = GameObject.Instantiate(Prefab, Parent);
                asteroid.transform.position = f.position;
                asteroid.transform.localScale = new Vector3(f.size, f.size, 1.0f);
                asteroid.GetComponent<Asteroid>().needInitialization = false;
                asteroid.GetComponent<IHasChemicalProfile>().setChemicalProfile(f.chemicalProfile);
                asteroid.GetComponent<Rigidbody2D>().mass = f.mass;
                asteroid.GetComponent<Rigidbody2D>().velocity = f.velocity;
            });

        yield return null;
    }
}