﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public long[] years = new[]
    {
        0L, 100L, 200L, 300L, 400L, 500L
    };

    public int yearToRotate = 0;

    public long timeStep = 100L;

    public float timeScale = 1f;

    public bool cannotReverse = false;

    public Coroutine currentTimeMachineJob;

    public TimeMachine timeMachine;
    
    public float gravityMultiplier = 1E+5f;

    public Transform mainPool;

    public List<string> poolTags;
    public List<string> PoolTags => poolTags;

    public GameObject player;
    public GameObject Player => player;

    public float globalCoolDown = 0.5f;

    public IEnumerator<(Transform, (int, int))> cameraTarget;

    public bool isCameraMoving = false;
    public bool isCameraZooming = false;
    public bool isMenuInFront = false;

    public Vector2 activeArea;

    public float maxFavor = 1000;
    public float actualFavor;

    public float constellationMiniGameTicker = 20f;

    [SerializeField]
    public ChemicalProfile targetProfile;
    
    public Dictionary<GradientTarget, Color[]> playerPalettes = new Dictionary<GradientTarget, Color[]>()
    {
        {GradientTarget.Planet, GradientTarget.Planet.baseColors()},
        {GradientTarget.Rings, GradientTarget.Rings.baseColors()}
    };
    
    public Dictionary<MilestoneType, Milestone> milestones = new Dictionary<MilestoneType, Milestone>();

    static GameManager()
    {
    }

    private GameManager()
    {
    }

    private void Awake()
    {
        instance = this;
        cameraTarget = getNextCameraTarget().GetEnumerator();
        cameraTarget.MoveNext();
        targetProfile = new ChemicalProfile(new Dictionary<ChemicalElement, float>());

        for (int i = 0; i < MilestoneTypeHandler.milestoneTypes.Count; i++)
        {
            if (!milestones.ContainsKey(MilestoneTypeHandler.milestoneTypes[i]))
            {
                milestones.Add(MilestoneTypeHandler.milestoneTypes[i], new Milestone(MilestoneTypeHandler.milestoneTypes[i], Constants.MILESTONE_DEFAULT_CAP));
            }
        }

        actualFavor = maxFavor;
    }

    private void Update()
    {
        cannotReverse = years.Contains(0);
    }

    public void updateFavor(float amount)
    {
        actualFavor = Mathf.Clamp(actualFavor + amount, 0, maxFavor);
        
        EventManager.RaiseFavorUsageEvent(actualFavor);
    }

    public void lifecycle()
    {
        var lastYear = years.Last();
        years = Utils<long>.shift(years);
        years[years.Length - 1] = lastYear + timeStep;
    }

    public static GameManager Instance
    {
        get { return instance; }
    }

    public Transform MainPool
    {
        get => mainPool;
        set => mainPool = value;
    }
    
    private IEnumerable<int> GetFibonacci()
    {
        int previousVal1 = 0;
        int previousVal2 = 1;

        while (true)
        {
            int nextVal = previousVal1 + previousVal2;
            previousVal1 = previousVal2;
            previousVal2 = nextVal;
            yield return nextVal;
        }
    }

    private IEnumerable<(Transform, (int, int))> getNextCameraTarget()
    {
        int count = 0;

        while (true)
        {
            yield return count % 2 == 0
                ? (player.transform, (10, 25))
                : (Utils.getNearestTransformFrom(player.transform.position,
                    GameObject.FindWithTag("StarsPool").transform), (100, 150));
            count++;
        }
    }
}