﻿using System;
using UnityEngine;

public class TextureGenerator : MonoBehaviour
{
    // Start is called before the first frame update
    
    private Vector2 HASH_X_PROJECTOR = new Vector2(127.1f, 311.7f);
    private Vector2 HASH_Y_PROJECTOR = new Vector2(269.5f, 183.3f);
    private const float HASH_SIN_MULTIPLIER = 43758.5453123f;
    private const float NOISE_K1 = 0.366025404f; // (sqrt(3)-1)/2;
    private const float NOISE_K2 = 0.211324865f; // (3-sqrt(3))/6;

    public float Scale = 16.0f;

    Vector2 hash(Vector2 p)
    {
        var tmp = new Vector2(Vector2.Dot(p, HASH_X_PROJECTOR), Vector2.Dot(p, HASH_Y_PROJECTOR));
        return
            new Vector2(-1.0f, -1.0f)
            + 2.0f * fract(new Vector2(Mathf.Sin(tmp.x) * HASH_SIN_MULTIPLIER, Mathf.Sin(tmp.y) * HASH_SIN_MULTIPLIER));
    }

    Vector2 fract(Vector2 p)
    {
        return p - new Vector2(Mathf.Floor(p.x), Mathf.Floor(p.y));
    }

    float step(float edge, float x)
    {
        return x < edge ? 0.0f : 1.0f;
    }

    float noise(Vector2 p)
    {
        Vector2 i = new Vector2(Mathf.Floor(p.x + (p.x + p.y) * NOISE_K1), Mathf.Floor(p.y + (p.x + p.y) * NOISE_K1));
        Vector2 a = p - i + Vector2.one * (i.x + i.y) * NOISE_K2;
        float m = step(a.y, a.x);
        Vector2 o = new Vector2(m, 1.0f - m);
        Vector2 b = a - o + Vector2.one * NOISE_K2;
        Vector2 c = a - Vector2.one + Vector2.one * 2 * NOISE_K2;

        Vector3 h = Vector3.Max(
            0.5f * Vector3.one 
            - new Vector3(Vector2.Dot(a, a), Vector2.Dot(b, b), Vector2.Dot(c, c)),
            Vector3.zero);

        Vector3 n = Vector3.Scale(Vector3.Scale(Vector3.Scale(Vector3.Scale(h, h), h), h), 
            new Vector3( 
                Vector2.Dot(a, hash(i + Vector2.zero)), 
                Vector2.Dot(b, hash(i + o)), 
                Vector2.Dot(c, hash(i + Vector2.one)))
            );

        return Vector3.Dot(n, Vector3.one * 70.0f);
    }


    void Start()
    {
        DateTime initial = System.DateTime.Now;

        HASH_X_PROJECTOR += new Vector2(UnityEngine.Random.value, UnityEngine.Random.value) * 5;
        HASH_Y_PROJECTOR += new Vector2(UnityEngine.Random.value, UnityEngine.Random.value) * 5;
        
        var texture = generateTexture();
        
        GetComponent<Renderer>().material.SetTexture(Shader.PropertyToID("BaseTexture_ref"), texture);
        //Debug.Log($"Elapsed to create the texture {(System.DateTime.Now - initial).TotalMilliseconds}ms");
    }

    public Texture2D generateTexture()
    {
        var texture = new Texture2D(64, 64, TextureFormat.RGB24, false);

        Color[] cells = new Color[64 * 64];
        
        for (int i = 0; i < 32; i++)
        {
            for (int j = 0; j < 64; j++)
            {
                float c = getPixelColor(new Vector2(i, j), new Vector2(64, 64));
                cells[i + 64 * j] = new Color(c, c, c);
            }
        }

        for (int i = 31; i >= 0; i--)
        {
            for (int j = 0; j < 64; j++)
            {
                cells[63 - i + 64 * j] = cells[i + 64 * j];
            }
        }
        
        texture.SetPixels(cells);

        // Apply all SetPixel calls
        texture.Apply();

        return texture;
    }
    private float getPixelColor(Vector2 position, Vector2 res)
    {
        Vector2 p = Vector2.Scale(position, new Vector2(1 / res.x, 1 / res.y));
        Vector2 uv = Vector2.Scale(p, Vector2.one);

        float f = 0.0f;
        
        if (p.x < 1.0f)
        {
            f = noise(Scale * uv);
        }
        else
        {
            uv = uv * 5.0f;
            Matrix4x4 m = new Matrix4x4(
                new Vector4(1.6f, 1.2f, 0f, 0f),
                new Vector4(-1.2f, 1.6f, 0f,0f),
                Vector4.zero,
                Vector4.zero
                );
            f = 0.5f * noise(uv);
            uv = m * uv;
            f += 0.25f * noise(uv);
            uv = m * uv;
            f += 0.125f * noise(uv);
            uv = m * uv;
            f += 0.0625f * noise(uv);
        }

        f = 0.5f + 0.5f * f;

        //f *= Mathf.SmoothStep(0.0f, 0.005f, Mathf.Abs(p.x - 0.6f));
        
        return  f;
    }

    private void OnValidate()
    {
        if (Application.isPlaying)
        {
            var texture = generateTexture();
        
            GetComponent<Renderer>().material.SetTexture(Shader.PropertyToID("BaseTexture_ref"), texture);
        }
    }
}