using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GradientTransformer : MonoBehaviour
{

    [Serializable]
    public struct GradientTransition
    {
        [SerializeField]
        public GradientTarget _target;
        public float time;
    }

    [SerializeField]
    public List<GradientTransition> transitionTimes = new List<GradientTransition>();

    private void Start()
    {
        //Register this planet to the gradientManager
        GradientManager.registerPlanet(
            gameObject.GetInstanceID(), 
            transitionTimes.ToDictionary(k => k._target, v => v.time)
            );
    }

    private void OnEnable()
    {
        //Register the event to the handler
        EventManager.changeColorEvent += blendColor;
    }

    private void OnDisable()
    {
        EventManager.changeColorEvent -= blendColor;
    }

    public void blendColor(GradientTarget target, ChemicalProfile profile)
    {
        GradientManager.DefaultTransformer[target]
            .transformerInfo
            ._queue 
            .Enqueue(GradientManager.DefaultTransformer[target].fromProfileToPalette(profile));

        if (GradientManager.DefaultTransformer[target].transformerInfo._coroutine == null)
        {
            GradientManager.DefaultTransformer[target].transformerInfo
                .setCoroutine(StartCoroutine(blendOverFrames(target)));    
        }
    }

    IEnumerator blendOverFrames(GradientTarget target)
    {
        while (GradientManager.DefaultTransformer[target].transformerInfo._queue.Count > 0)
        {
            Color[] targetPalette = GradientManager.DefaultTransformer[target].transformerInfo._queue.Dequeue();

            do
            {
                yield return new WaitForFixedUpdate(); //Run this every FixedUpdate
            } while (GradientManager.DefaultTransformer[target]
                .gradientMaterialTransformerProcessor(GetComponent<Renderer>().material, targetPalette));
        }
        
        GradientManager.DefaultTransformer[target].transformerInfo.setCoroutine(null);
    }
}