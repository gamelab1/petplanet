﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class GradientGenerator : MonoBehaviour
{
    private int colorsCount = 2;
    private Vector2 colorDistance = new Vector2(12, 3);

    public Color colorA;
    public Color colorB;
    public Color colorC;

    public (Color, Color, Color) defineAndApplyColors()
    {
        var colors = getColors();
        
        colorA = colors.Item1;
        colorB = colors.Item2;
        colorC = colors.Item3;
        
        GetComponent<Renderer>().material.SetColor(Shader.PropertyToID("GradientColor_A_ref"), colors.Item1);
        GetComponent<Renderer>().material.SetColor(Shader.PropertyToID("GradientColor_B_ref"), colors.Item2);
        GetComponent<Renderer>().material.SetColor(Shader.PropertyToID("GradientColor_C_ref"), colors.Item3);

        return (colorA, colorB, colorC);
    }

    public (Color, Color, Color) getColors()
    {
        Vector3 hsvColor = gameObject.GetComponent<IHasChemicalProfile>() != null &&
                           gameObject.GetComponent<IHasChemicalProfile>().getChemicalProfile().ChemicalMap != null
            ? gameObject.GetComponent<IHasChemicalProfile>().getChemicalProfile().max().getTransformedHSV()
            : new Vector3(Random.value,  Random.Range(0.6f, 0.85f), Random.Range(0.7f, 0.75f));
        
        var (hue, s, v) = (hsvColor.x, hsvColor.y, hsvColor.z);
        
        return (
            Color.HSVToRGB(hue, s, v),
            Color.HSVToRGB(hue, Mathf.Clamp(s + Random.Range(-0.1f, -0.2f), 0, 1), Mathf.Clamp(v + Random.Range(0.1f, 0.2f), 0, 1)),
            Color.HSVToRGB(hue, Mathf.Clamp(s + Random.Range(-0.1f, -0.2f), 0, 1), Mathf.Clamp(v + Random.Range(-0.1f, -0.2f), 0, 1))
            );
    }

    public Gradient getGradient()
    {
        var tmpGradient = new Gradient();
        
        GradientColorKey[] colorKey;
        GradientAlphaKey[] alphaKey;
        
        int colorGroups = colorsCount / 2;
        bool hasExtraColor = colorsCount % 2 == 1;
        
        // Populate the color keys at the relative time 0 and 1 (0 and 100%)
        colorKey = new GradientColorKey[colorGroups * 4 + colorsCount % 2];

        List<(Color, Color)> colors = Enumerable.Range(1, colorGroups).Select(colorG =>
        {
            float hue = Random.Range(0.0f, 1.0f);
            float s = Random.Range(0.65f, 0.80f);
            float v = Random.Range(0.7f, 1.0f);
            return (Color.HSVToRGB(hue, s, v),
                Color.HSVToRGB(hue, s + Random.Range(-0.3f, 0.3f), v + Random.Range(-0.3f, 0.3f)));
        }).ToList();

        float min = 0.0f;
        float max = 1.0f;
        
        foreach (var ((color1, color2), i) in colors.Zip(Enumerable.Range(0, colorGroups), (tuple, i) => (tuple, i))
            .ToList())
        {
            var xScaled = (max - min) * colorDistance.x / 100.0f;
            var yScaled = (max - min) * colorDistance.y / 100.0f;

            colorKey[i].color = color1;
            colorKey[i].time = min + xScaled;

            colorKey[i + 1].color = color2;
            colorKey[i + 1].time = colorKey[i].time + yScaled;

            colorKey[colorKey.Length - i - 1].color = color1;
            colorKey[colorKey.Length - i - 1].time = max - xScaled;
            colorKey[colorKey.Length - i - 2].color = color2;
            colorKey[colorKey.Length - i - 2].time = max - xScaled - yScaled;

            max = colorKey[colorKey.Length - i - 2].time;
            min = colorKey[i + 1].time;
        };

        if (hasExtraColor)
        {
            colorKey[(colorKey.Length - 1) / 2].color = Color.blue;
            colorKey[(colorKey.Length - 1) / 2].time = 0.5f;
        }
        
        // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
        alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1.0f;
        alphaKey[1].time = 1.0f;

        tmpGradient.SetKeys(colorKey, alphaKey);
        tmpGradient.mode = GradientMode.Blend;
        
        return tmpGradient;
    }

    private void OnValidate()
    {
        if (Application.isPlaying)
        {
            defineAndApplyColors();
        }
    }
}
