using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GradientManager
{

    private static readonly Dictionary<int, Dictionary<GradientTarget, GradientUtils.GradientTransformerHandler>>
        _gradientTransformerHandlers =
            new Dictionary<int, Dictionary<GradientTarget, GradientUtils.GradientTransformerHandler>>();

    public static Dictionary<int, Dictionary<GradientTarget, GradientUtils.GradientTransformerHandler>> Transformers
    {
        get => _gradientTransformerHandlers;
    }

    public static Dictionary<GradientTarget, GradientUtils.GradientTransformerHandler>
        DefaultTransformer
    {
        get => _gradientTransformerHandlers[_gradientTransformerHandlers.Keys.First()];
    }

    public static void registerPlanet(int id, Dictionary<GradientTarget, float> transitionTimes)
    {
        _gradientTransformerHandlers.Add(
            id,
            new Dictionary<GradientTarget, GradientUtils.GradientTransformerHandler>()
            {
                {
                    GradientTarget.Planet, new GradientUtils.GradientTransformerHandler(
                        (material, colors) =>
                            GradientUtils.applyTransform(material, colors, GradientTarget.Planet),
                        (profile) => GradientUtils.fromColorToPalette(profile.max().getColor()),
                        new GradientUtils.GradientTransformerInfo(
                            new Queue<Color[]>(),
                            null,
                            transitionTimes[GradientTarget.Planet]
                        )
                    )
                },

                {
                    GradientTarget.Rings, new GradientUtils.GradientTransformerHandler(
                        (material, colors) =>
                            GradientUtils.applyTransform(material, colors, GradientTarget.Rings),
                        (profile) => new[] {profile.min().getColor(), profile.max().getColor()},
                        new GradientUtils.GradientTransformerInfo(
                            new Queue<Color[]>(),
                            null,
                            transitionTimes[GradientTarget.Rings]
                        )
                    )
                }
            }
        );
    }
}