﻿using UnityEngine;
using Utilities;

public class Gravity : MonoBehaviour
{
    public Vector2 forceG = Vector2.zero;

    private Rigidbody2D _rigidbody2D;

    public ForceMode2D m_forceMode;

    public Vector2 initialForce;

    public bool applyForceToPosition = false;

    public float gravityFieldRadius = 1000f;

    public float realMass;

    private void Awake()
    {
        GetComponent<Rigidbody2D>().mass = Mathf.Max(GetComponent<Rigidbody2D>().mass, realMass);
    }

    // Start is called before the first frame update
    private void Start()
    {
        _rigidbody2D = gameObject.GetComponent<Rigidbody2D>();

        var tmp = Constants.GRAVITATIONAL_MULTIPLIER
                  * (new Vector2(
                      initialForce.x + (UnityEngine.Random.value - 0.5f) * 0.05f,
                      initialForce.y + (UnityEngine.Random.value - 0.5f) * 0.05f)
                  );

        gravityFieldRadius *= Mathf.Max(GetComponent<Rigidbody2D>().mass, realMass) / 1E6f;

        _rigidbody2D.AddForce(
            tmp,
            ForceMode2D.Impulse);
    }

    private void FixedUpdate()
    {
        //Take every object inside a circle of radius R
        ////calculate Fg for each object
        // 00000001 << 8 => 10000000
        // 00000010 << 7 => 10000000
        int layerMask = 1 << 8;

        RaycastHit2D[] hits = Physics2D.CircleCastAll(
            transform.position,
            gravityFieldRadius,
            Vector2.zero,
            gravityFieldRadius,
            layerMask
        );

        for (int i = 0; i < hits.Length; i++)
        {
            var rigidBodyHit = hits[i].transform.GetComponent<Rigidbody2D>();
            rigidBodyHit.AddForce(
                -GameManager.Instance.gravityMultiplier
                * Utils.getForceG(
                    _rigidbody2D,
                    rigidBodyHit,
                    GetComponent<IHasColor>() != null ? GetComponent<IHasColor>().getColor() : Color.blue)
            );
        }
        //
        // Physics2D.CircleCastAll(
        //         transform.position,
        //         gravityFieldRadius,
        //         Vector2.zero,
        //         gravityFieldRadius,
        //         layerMask
        //     ).Select(hit2D => hit2D.transform.GetComponent<Rigidbody2D>())
        //     .ToList()
        //     .ForEach(r =>
        //     {
        //         r.AddForce(
        //             -GameManager.Instance.gravityMultiplier
        //             * Utils.getForceG(
        //                 _rigidbody2D,
        //                 r.GetComponent<Rigidbody2D>(),
        //                 GetComponent<IHasColor>() != null ? GetComponent<IHasColor>().getColor() : Color.blue)
        //         );
        //     });

        forceG = _rigidbody2D.velocity;

        Debug.DrawRay(
            transform.position,
            forceG.normalized * (10 * forceG.magnitude),
            GetComponent<IHasColor>() != null ? GetComponent<IHasColor>().getColor() : Color.blue
        );
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, gravityFieldRadius);
    }
}