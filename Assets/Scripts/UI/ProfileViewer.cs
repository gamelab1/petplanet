﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ProfileViewer : MonoBehaviour
{
    private ChemicalProfile _chemicalProfile;
    public GameObject rowPrefab;

    public Transform rowContainer;

    public List<GameObject> rows;
    
    private void Start()
    {
        _chemicalProfile = GameManager.Instance.player.GetComponent<Planet>().chemicalProfile;

        var containerHeight = _chemicalProfile.ChemicalElements.Count * 300;
        
        rowContainer.GetComponent<RectTransform>().sizeDelta =
            new Vector2(0, containerHeight);
        
        rowContainer.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        rows = _chemicalProfile.ChemicalElements
            .Select((e , index) => toChemicalElementRow(e, containerHeight, index))
            .ToList();
    }

    private void Update()
    {
        rows.ForEach(r =>
        {
            r.GetComponent<ChemicalElementUIRow>().elementLevel.GetComponent<RectTransform>().localScale =
                new Vector3(
                    Mathf.Clamp(_chemicalProfile.ChemicalMap[r.GetComponent<ChemicalElementUIRow>().element], 0f, 1f),
                    1f, 
                    1f);
        });
    }

    private GameObject toChemicalElementRow(ChemicalElementTuple chemicalElementTuple, float containerHeight, int offsetMultiplier)
    {
        var tmp = GameObject.Instantiate(rowPrefab, rowContainer);

        var objTransform = tmp.GetComponent<RectTransform>();

        ChemicalElementUIRow uiRow = tmp.GetComponent<ChemicalElementUIRow>();

        uiRow.element = chemicalElementTuple._chemicalElement;
        uiRow.elementLevel.color = chemicalElementTuple.Color;
        uiRow.elmentImage.color = chemicalElementTuple.Color;
        uiRow.elementImageAltText.text = chemicalElementTuple._chemicalElement + "";
        uiRow.elementLevel.GetComponent<RectTransform>().localScale = new Vector3(
            Mathf.Clamp(1f * chemicalElementTuple.amount, 0f, 1f),
            1f,
            1f
        );

        objTransform.anchoredPosition = new Vector3(
            0f, 
            containerHeight / 2 + (-1 * offsetMultiplier * objTransform.rect.height),
            0f
            );
        
        return tmp;
    }
}
