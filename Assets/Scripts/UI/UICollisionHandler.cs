using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class UICollisionHandler : MonoBehaviour
{
    [SerializeField] private GameObject collisionTextPrefab = null;

    public Transform collisionUIObjectPool;

    [SerializeField] private List<GameObject> collisionUIObjects = new List<GameObject>();

    private IEnumerator<GameObject> collisionUIObjectGenerator;

    public float radialStrenghtMultiplier = 1f;
    public float offeset = 1f;
    public float timeToLive = 1f;

    private void OnEnable()
    {
        //Register to event
        EventManager.incomingCollisionEvent += onCollisionInstance;
    }

    private void OnDisable()
    {
        //Unregister to event
        EventManager.incomingCollisionEvent -= onCollisionInstance;
    }

    private void Start()
    {
        collisionUIObjects = Utilities.Utils<Transform>.ToIEnumerable(collisionUIObjectPool.GetEnumerator())
            .Select(t => t.gameObject)
            .ToList();

        collisionUIObjectGenerator = getNextAvailableUICollisionObject();
    }

    public void onCollisionInstance(Vector2 position, ChemicalProfile profile, float mass)
    {

        Vector2 playerPos = GameManager.Instance.player.transform.position;

        Vector2 targetPosition = position + (position - playerPos).normalized * offeset;

        collisionUIObjectGenerator.MoveNext();

        var textObject = collisionUIObjectGenerator.Current;

        textObject.transform.position = targetPosition;

        RectTransform rectTransform = textObject.GetComponent<RectTransform>();

        rectTransform.pivot = (position.x - playerPos.x >= 0 ? Vector2.zero : Vector2.right)
                              + (position.y - playerPos.y >= 0 ? Vector2.zero : Vector2.up);

        var rigidbody = textObject.GetComponent<Rigidbody2D>();

        ChemicalElement max = profile.max();

        textObject.GetComponent<TextMeshPro>().text = String.Format("+{0} {1}", profile.ChemicalMap[max] * mass, max);
        
        textObject.GetComponent<Renderer>().material.SetColor(
            Shader.PropertyToID(Constants.UI_COLLISION_FACE_COLOR_REF), 
            max.getColor()
            );
        
        textObject.GetComponent<MeshRenderer>().enabled = true;

        rigidbody.velocity = GameManager.Instance.player.GetComponent<Rigidbody2D>().velocity;

        rigidbody.AddForce((position - ((Vector2) GameManager.Instance.player.transform.position)).normalized *
                           radialStrenghtMultiplier);
        
        //Let it hide after X seconds
        StartCoroutine(hide(textObject));
    }

    public IEnumerator hide(GameObject g)
    {
        yield return new WaitForSeconds(timeToLive);
        g.GetComponent<MeshRenderer>().enabled = false;
    }
    
    private IEnumerator<GameObject> getNextAvailableUICollisionObject()
    {
        while (true)
        {
            var selected = collisionUIObjects.FirstOrDefault(g => !g.GetComponent<MeshRenderer>().enabled);
            
            if (selected == null)
            {
                var newly = GameObject.Instantiate(collisionTextPrefab, Vector3.zero, Quaternion.identity,
                    collisionUIObjectPool);
                newly.GetComponent<MeshRenderer>().enabled = false;
                collisionUIObjects.Add(newly);
                selected = newly;
            }

            yield return selected;
        }
    }
}