using UnityEngine;
using DG.Tweening;

public class FavorBar : MonoBehaviour
{
    private RectTransform _rectTransform;
    public float timeToTween = 1f;

    // Start is called before the first frame update
    void Start()
    {
        _rectTransform = this.GetComponent<RectTransform>();
    }

    private void OnEnable()
    {
        EventManager.favorUsageEvent += updateBar;
    }

    private void OnDisable()
    {
        EventManager.favorUsageEvent -= updateBar;
    }

    public void updateBar(float favor)
    {
        var ratio = favor / GameManager.Instance.maxFavor;
        
        _rectTransform.DOScale(new Vector3(ratio, 1,1), timeToTween);
    }
}
