﻿using UnityEngine;
using UnityEngine.UI;

public class ChemicalElementUIRow : MonoBehaviour
{
    public Image elmentImage;
    public Text elementImageAltText;
    public Image elementLevel;
    public ChemicalElement element;
}
