using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject toastTemplate;

    public Transform canvas;

    private Dictionary<UIElementType , IEnumerator<GameObject>> UIObjectGenerator;

    public List<GameObject> UIObjects;
    public Transform toastPool;
    
    private void OnEnable()
    {
        EventManager.UICreateCommandEvent += createToast;
    }

    private void OnDisable()
    {
        EventManager.UICreateCommandEvent -= createToast;
    }

    private void Start()
    {
        UIObjectGenerator = new Dictionary<UIElementType, IEnumerator<GameObject>>
        {
            {UIElementType.Toast, getNextAvailableToastsUIObject()}
        };
    }

    public void createToast<T>(String text, float ttl, Action<T> callback, T target)
    {
        UIObjectGenerator[UIElementType.Toast].MoveNext();
        var toastObject = UIObjectGenerator[UIElementType.Toast].Current;

        Text toastText = toastObject.transform.GetChild(0).GetComponent<Text>();

        toastText.enabled = true;
        toastObject.GetComponent<Image>().enabled = true;

        toastText.text = text.ToUpper().Substring(0, 1) + text.Substring(1).Trim();

        StartCoroutine(disableAfter(toastObject, ttl, callback, target));
    }

    IEnumerator disableAfter<T>(GameObject toastObject, float ttl, Action<T> callback, T target)
    {
        yield return new WaitForSeconds(ttl);

        toastObject.GetComponent<Animator>().SetTrigger("FadeOut");

        callback.Invoke(target);
    }
    
    private IEnumerator<GameObject> getNextAvailableToastsUIObject()
    {
        while (true)
        {
            var selected = UIObjects.FirstOrDefault(g => !g.GetComponent<Image>().enabled);
            
            if (selected == null)
            {
                var newly = GameObject.Instantiate(toastTemplate, Vector3.zero, Quaternion.identity,
                    toastPool);
                newly.GetComponent<MeshRenderer>().enabled = false;
                UIObjects.Add(newly);
                selected = newly;
            }

            yield return selected;
        }
    }
}
