using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class PostProcessingEffectsHandler : MonoBehaviour
{
    private Volume _volume;

    private Dictionary<String, VolumeComponent> _components = new Dictionary<string, VolumeComponent>();
    
    // Start is called before the first frame update
    void Start()
    { 
        _volume = GetComponent<Volume>();

        _components = _volume.profile.components
            .ToDictionary(k => k.name, v => v);
    }

    private void OnEnable()
    {
        EventManager.toggleVolumetricEffectEvent += toggleVFX;
    }

    private void OnDisable()
    {
        EventManager.toggleVolumetricEffectEvent -= toggleVFX;
    }

    void toggleVFX(bool active, String effect, float intensity)
    {
        if (_components.ContainsKey(effect))
        {
            _components[effect].active = active;
        }
    }
    
    void toggleVFX<T>(bool active, String effect, Action<T> action)
    {
        if (_components.ContainsKey(effect))
        {
            List<VolumeComponent> tmp = new List<VolumeComponent>()
            {
                _components[effect]
            };
            
            tmp[0].active = active;

            if (tmp[0].active)
            {
                T typedComponent = tmp.Cast<T>().First();
                
                action.Invoke(typedComponent);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
