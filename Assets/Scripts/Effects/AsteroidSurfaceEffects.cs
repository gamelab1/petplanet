﻿using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidSurfaceEffects : MonoBehaviour
{
    public float complexityScaleMaxVariance = 5.0f;
    
    
    // Start is called before the first frame update
    void Start()
    {
        Material m = GetComponent<Renderer>().material;

        ChemicalProfile chemicalProfile = GetComponent<IHasChemicalProfile>().getChemicalProfile();

        m.SetColor(Shader.PropertyToID("DetailColor_ref"), chemicalProfile.max().getColor());

        var complexityScale = m.GetFloat(Shader.PropertyToID("ComplexityScale_ref")) -
                              Random.value * complexityScaleMaxVariance;
        
        m.SetFloat(Shader.PropertyToID("ComplexityScale_ref"), Mathf.Min(7.0f, Mathf.Max(2.0f, complexityScale)));
        m.SetFloat(Shader.PropertyToID("ComplexityRotation_ref"), Random.value * Mathf.PI * 2);
        m.SetVector(Shader.PropertyToID("CratersComplexity_ref"), new Vector2(
            Random.value * 10f,
            Random.value * 10f
            ));
    }
}
