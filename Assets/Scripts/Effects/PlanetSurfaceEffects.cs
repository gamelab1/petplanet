﻿using System.Collections;
using UnityEngine;
using Utilities;

public class PlanetSurfaceEffects : MonoBehaviour
{
    public float m_UpperBoundCraterCount = 4f;
    public float m_LowerBoundCraterCount = 1.5f;

    public int m_GlobalHits;
    public float m_HitGarbageCollectorCron;
    public int m_MaxHitsCount = 100;

    public float remapped;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ForgetHit(m_HitGarbageCollectorCron));
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        m_GlobalHits = Mathf.Min(m_MaxHitsCount, ++m_GlobalHits);

        applyEffects();
    }

    public void applyEffects()
    {
        remapped = Utils.remap(
            m_MaxHitsCount - m_GlobalHits,
            (0, m_MaxHitsCount),
            (m_LowerBoundCraterCount, m_UpperBoundCraterCount)
        );

        GetComponent<Renderer>().material.SetFloat(
            Shader.PropertyToID("CraterCountDivide_ref"),
            Mathf.Min(Mathf.Max(m_LowerBoundCraterCount, remapped), m_UpperBoundCraterCount)
        );
    }

    IEnumerator ForgetHit(float time)
    {
        while (true)
        {
            yield return new WaitForSeconds(time);
            m_GlobalHits = Mathf.Max(0, --m_GlobalHits);
        }
    }
}