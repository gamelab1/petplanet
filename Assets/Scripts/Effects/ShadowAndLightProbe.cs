﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;

public class ShadowAndLightProbe : MonoBehaviour
{
    public List<Transform> stars = new List<Transform>();
    
    public float finalAngle;

    private Renderer _renderer;
    private float _totalStarsMass;
    public float K;
    public float intensity;
    
    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<Renderer>();

        if (stars.Count == 0)
        {
            stars = Utils<Transform>
                .ToIEnumerable(GameObject.FindGameObjectWithTag("StarsPool").transform.GetEnumerator()).ToList();
        }

        _totalStarsMass = stars.Sum(s => s.GetComponent<Rigidbody2D>().mass);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        finalAngle = stars
            .Select(s => (
                getCosAngleFrom(transform, s,
                    Vector2.up), //Cosine of the angle between the planet and the star around the Y axis
                getCosAngleFrom(transform, s,
                    Vector2.right), //Cosine of the angle between the planet and the star around the X axis 
                s.GetComponent<Rigidbody2D>().mass, //Mass of the star
                s //Star transform
            ))
            .Aggregate((-1.0f, 0.0f), (i, tuple) =>
            {
                // if (i.Item1 < 0)
                // {
                //     return (tuple.Item2 < 0 ? 2 * Mathf.PI - Mathf.Acos(tuple.Item1) : Mathf.Acos(tuple.Item1),
                //         tuple.s.position.x);
                // }
                //
                // var angle = tuple.Item2 < 0 ? 2 * Mathf.PI - Mathf.Acos(tuple.Item1) : Mathf.Acos(tuple.Item1);
                // var xDistancePlanetFromNextStar = transform.position.x - tuple.s.position.x;
                // var xDistancePlanetFromPastStar = transform.position.x - i.Item2;
                //
                // var t = 0.0f;
                //
                // var right = xDistancePlanetFromNextStar < 0 && xDistancePlanetFromPastStar < 0;
                // var left = xDistancePlanetFromNextStar > 0 && xDistancePlanetFromPastStar > 0;
                //
                // if (right || left)
                // {
                //     t = Mathf.Abs(xDistancePlanetFromNextStar) <= Mathf.Abs(xDistancePlanetFromPastStar) ? 1.0f : 0.0f;
                // }
                // else
                // {
                //     var xDistanceBetweenStars = Mathf.Abs(tuple.s.position.x - i.Item2);
                //     t = Mathf.Abs(xDistancePlanetFromPastStar) / xDistanceBetweenStars;
                //
                // }
                //
                // return (Mathf.LerpAngle(i.Item1, angle, t), tuple.s.position.x);
                return (tuple.Item2 < 0 ? 2 * Mathf.PI - Mathf.Acos(tuple.Item1) : Mathf.Acos(tuple.Item1), 0.0f);
            }).Item1;

        intensity = stars.Sum(s => 
            K * Constants.INTENSITY_MULTIPLIER * _totalStarsMass / 
            (s.GetComponent<Rigidbody2D>().mass * Mathf.Pow((s.position - transform.position).magnitude, 2)));

        _renderer.material.SetFloat(
            Shader.PropertyToID("ShadowDirection_ref"),
            -finalAngle
            );
        
        _renderer.material.SetFloat(
            Shader.PropertyToID("ShadowOpacity_ref"),
            intensity
            );
        
        
        _renderer.material.SetFloat(
            Shader.PropertyToID("ObjectRotation_ref"), 
            transform.rotation.eulerAngles.z);

#if UNITY_EDITOR
        var toDegree = Mathf.Rad2Deg * -finalAngle;

        Debug.DrawRay(
            transform.position, 
            Quaternion.AngleAxis(toDegree, Vector3.back) * Vector3.up * 10,
            Color.green,
            0.05f);
#endif
    }

    private float getCosAngleFrom(Transform reference, Transform other, Vector2 axis)
    {
        Vector2 up = axis;
        Vector2 distance = other.position - reference.position;

        return Vector2.Dot(up, distance) / (up.magnitude * distance.magnitude);
    }
}
