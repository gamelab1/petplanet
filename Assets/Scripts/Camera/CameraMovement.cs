﻿using System.Collections;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform targetPosition;
    public Transform temporaryTarget;

    private Vector3 cameraSize2D;
    public Vector3 targetCameraSize2D;
    
    private Vector3 distance;

    public float smoothTime = 0.3F;
    public Vector3 velocityMovement = Vector3.zero;
    public Vector3 velocitySize = Vector3.zero;

    public bool isMovingTowards = false;
    public bool isZooming = false;
    public float timeToMove;
    public float timeToZoom;

    public float thresholdDistance = 0.3f;
    public float thresholdZoom = 0.2f;

    private Camera camera;

    public float zoomTime = 0.5f;

    public bool alteredZoom = false;
    
    private void OnEnable()
    {
        EventManager.pinchEvent += onPinch;
    }

    private void OnDisable()
    {
        EventManager.pinchEvent -= onPinch;
    }

    private void onPinch(float pinchAmount)
    {
        if (!isMovingTowards && !isZooming)
        {
            // Debug.Log(string.Format("Camera is at size: {0} and should go to {1} detected a pinch of {2}", 
            //     camera.orthographicSize, 
            //     targetCameraSize2D.x,
            //     pinchAmount));

            var (normalLevel, alteredLevel) = GameManager.Instance.cameraTarget.Current.Item2;
            
            if (alteredZoom && pinchAmount > 0)
            {
                StartCoroutine(zoomTo(Vector3.right * normalLevel, zoomTime));
                alteredZoom = !alteredZoom;
            }

            if (!alteredZoom && pinchAmount < 0)
            {
                StartCoroutine(zoomTo(Vector3.right * alteredLevel, zoomTime));
                alteredZoom = !alteredZoom;
            }
           
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        distance = gameObject.transform.position - targetPosition.position;
        cameraSize2D = Vector3.right * GetComponent<Camera>().orthographicSize;
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(
            transform.position,
            targetPosition.transform.position - transform.position,
            Color.green);

        if (isZooming || isMovingTowards)
        {
            if (isMovingTowards)
            {
                transform.position = Vector3.SmoothDamp(
                    transform.position, 
                    targetPosition.transform.TransformDirection(distance) + temporaryTarget.position, 
                    ref velocityMovement, 
                    timeToMove);
            }

            if (isZooming)
            {
                camera.orthographicSize = Vector3.SmoothDamp(
                    camera.orthographicSize * Vector3.right, 
                    targetCameraSize2D, 
                    ref velocitySize, 
                    timeToZoom).x;

                if (!isMovingTowards)
                {
                    transform.position = targetPosition.transform.TransformDirection(distance) + targetPosition.transform.position;
                    transform.LookAt(targetPosition);
                }
            }
        }
        else
        {
            transform.position = targetPosition.transform.TransformDirection(distance) + targetPosition.transform.position;
            transform.LookAt(targetPosition);
        }
    }

    public IEnumerator zoomTo(Vector3 targetZoomSize, float time)
    {
        isZooming = true;
        GameManager.instance.isCameraZooming = true;
        timeToZoom = time;
        targetCameraSize2D = targetZoomSize;
        
        yield return new WaitUntil(() =>
            Mathf.Abs(targetZoomSize.x - camera.orthographicSize) < thresholdZoom);

        isZooming = false;
        GameManager.instance.isCameraZooming = false;
        camera.orthographicSize = targetZoomSize.x;
        timeToZoom = 0;
    }

    public IEnumerator moveToPosition(Transform destination, CircleCollider2D objectToFrame, float time)
    {
        isMovingTowards = true;
        isZooming = true;
        GameManager.instance.isCameraMoving = true;
        GameManager.instance.isCameraZooming = true;
        timeToMove = time;
        timeToZoom = time;
        temporaryTarget = destination;
        var oldCameraPosition = camera.transform.position;
        var oldCameraSize = camera.orthographicSize;
        camera.transform.position =
            new Vector3(destination.position.x, destination.position.y, camera.transform.position.z);

        targetCameraSize2D = cameraSize2D;
        

        while (!testIfInsideCameraFrame(destination.position, objectToFrame.radius * destination.localScale.x, Vector2.one * 270))
        {
            targetCameraSize2D += Vector3.right * 5f;
            camera.orthographicSize = targetCameraSize2D.x;
        }
        
        camera.orthographicSize = oldCameraSize;
        camera.transform.position = oldCameraPosition;
        
        //Trigger VFX event to get Motion Blur on
        EventManager.RaiseToggleVolumetricEffectEvent(true, Constants.MOTION_BLUR_NAME, 1.0f);
        
        yield return new WaitUntil(() =>
            ((Vector2) destination.position - (Vector2) transform.position).magnitude < thresholdDistance);
            
        //Trigger VFX event to get Motion Blur off
        EventManager.RaiseToggleVolumetricEffectEvent(false, Constants.MOTION_BLUR_NAME, 1.0f);
        
        isMovingTowards = false;
        isZooming = false;
        GameManager.instance.isCameraMoving = false;
        GameManager.instance.isCameraZooming = false;
        targetPosition = objectToFrame.transform;
        timeToMove = 0;
        timeToZoom = 0;
    }

    private bool testIfInsideCameraFrame(Vector3 pos, float scaledRadius, Vector2 padding)
    {
        var screenTargetPos = camera.WorldToScreenPoint(pos);

        var screenRadiusPos = camera.WorldToScreenPoint(pos + Vector3.right * scaledRadius);

        var screenRadius = screenRadiusPos.x - screenTargetPos.x;
        
        return screenRadius * 2 + padding.x * 2 < camera.pixelWidth ;
    }
    
}
