using System;
using System.Collections.Generic;
using UnityEngine;

public class MilestonesManager : MonoBehaviour
{
    private void OnEnable()
    {
        EventManager.milestoneCheckEvent += verifyAndConfirmMilestone;
    }

    private void OnDisable()
    {
        EventManager.milestoneCheckEvent -= verifyAndConfirmMilestone;
    }

    public void verifyAndConfirmMilestone(MilestoneType milestoneType, Func<Boolean> predicate)
    {
        if (predicate.Invoke())
        {
            if (GameManager.Instance.milestones.ContainsKey(milestoneType))
            {
                Milestone milestone = GameManager.Instance.milestones[milestoneType];

                int currentLevel = milestone.Level;
                
                if(milestoneType.getLevelCheckMap().Length > 0 && milestoneType.getLevelCheckMap()[currentLevel].Invoke())
                {
                    if (Mathf.Min(currentLevel + 1, milestone.LevelCap) > currentLevel)
                    {
                        EventManager.RaiseMilestoneLevelUpEvent(milestoneType, currentLevel + 1);
                    }
                    
                    GameManager.Instance.milestones[milestoneType] = GameManager.Instance.milestones[milestoneType].levelUp();
                }
            }
            else
            {
                EventManager.RaiseMilestoneLevelUpEvent(milestoneType, 0);
                GameManager.Instance.milestones.Add(milestoneType, new Milestone(milestoneType, Constants.MILESTONE_DEFAULT_CAP));
            }
        }
    }
}