using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonoBehaviourUtils : MonoBehaviour
{
    void SwitchMeOff()
    {
        EventManager.RaiseEnqueueGameObjectToDestroy(
            this.gameObject,
            g => Debug.Log($"Destroying {gameObject.name}"),
            3.0f
        );
        
        this.gameObject.SetActive(false);
    }

    void DisableToast()
    {
        this.GetComponent<Image>().enabled = false;

        foreach (var textField in this.GetComponentsInChildren<Text>())
        {
            textField.enabled = false;
        }
        
    }
}
