using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Generator
{
    private GameObject prefab;
    private Transform parent;
    private int maxNumElement = 0;
    private int generatorStep = 10;

    public Transform Parent
    {
        get => parent;
        set => parent = value;
    }

    public GameObject Prefab
    {
        get => prefab;
        set => prefab = value;
    }

    public int MAXNumElement
    {
        get => maxNumElement;
        set => maxNumElement = value;
    }

    public int GeneratorStep
    {
        get => generatorStep;
        set => generatorStep = value;
    }

    public Generator(GameObject g)
    {
        this.prefab = g;
    }

    protected Generator()
    {
        prefab = null;
    }

    public virtual Coroutine generate(MonoBehaviour m, float tick)
    {
        var tmp = m.StartCoroutine(generation(tick));

        return tmp;
    }
    public abstract IEnumerator generation(float tick);
    
    public virtual int getTargetNumOfNextGeneration()
    {
        var activeObjectCount = Parent.GetComponent<PoolStatus>().activeElements;
        var tmp = activeObjectCount + UnityEngine.Random.value * generatorStep;
        
        return (int) Mathf.Max(0,Mathf.Min(tmp, maxNumElement) - activeObjectCount);
    }

    public abstract IEnumerator burst<T>(IEnumerable<T> properties);
}
