using System;
using UnityEngine;
using UnityEngine.UI;

public class GeometryUtils
{
    public static Line getLineFor(Vector3 a, Vector3 b)
    {
        if (a.x != b.x)
        {
            return new Line((a.x * b.y - b.x * a.y) / (a.x - b.x), (a.y - b.y) / (a.x - b.x));
        }

        return new Line(a.x, 0, true);
    }

    public static Vector3 collide(Line a, Line b)
    {
        if (a.Coefficient == b.Coefficient)
        {
            throw new Exception("Lines are parallel");
        }

        return new Vector3(
            (b.Constant - a.Constant) / (a.Coefficient - b.Coefficient),
            a.Coefficient * (b.Constant - a.Constant) / (a.Coefficient - b.Coefficient) + a.Constant,
            0
        );
    }

    public static bool isInsideSegment(Vector3 test, Vector3 a, Vector3 b)
    {
        bool isInsideX = a.x < b.x
            ? test.x > a.x && test.x < b.x
            : test.x < a.x && test.x > b.x;
        
        bool isInsideY = a.y < b.y
            ? test.y > a.y && test.y < b.y
            : test.y < a.y && test.y > b.y;
        
        return isInsideX && isInsideY;
    }
    

    public struct Line
    {
        private float _coefficient;
        private float _constant;
        private bool _isParallelToY;

        public Line(float constant, float coefficient, bool isParallelToY = false)
        {
            this._constant = constant;
            this._coefficient = coefficient;
            this._isParallelToY = isParallelToY;
        }

        public float Coefficient
        {
            get => _coefficient;
            set => _coefficient = value;
        }

        public float Constant
        {
            get => _constant;
            set => _constant = value;
        }

        public bool IsParallelToY
        {
            get => _isParallelToY;
            set => _isParallelToY = value;
        }
    }
    
}