﻿using System.Collections.Generic;
using System.Linq;

public class Deque<T>
{
    private List<T> innerList;

    public Deque()
    {
        this.innerList = new List<T>();
    }

    public void Enqueue(T o)
    {
        innerList.Add(o);
    }

    public T Dequeue()
    {
        var item = innerList.First();
        innerList.RemoveAt(0);
        return item;
    }

    public T Pop()
    {
        var item = innerList.Last();
        innerList.RemoveAt(innerList.Count - 1);
        return item;
    }

    public T Peek(bool tail = false)
    {
        return !tail ? innerList.Last() : innerList.First();
    }

    public List<T> ToList()
    {
        return innerList;
    }

    public int Count
    {
        get => innerList.Count;
    }
}
