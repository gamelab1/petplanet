using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utilities
{
    public static class DarwinUtils
    {

        public static float fitness(ChemicalProfile solution)
        {
            ChemicalProfile target = GameManager.Instance.targetProfile;
            //Calcolare lo scarto quadratico medio per elemento tra la soluzione e il target
            return 1 - target.ChemicalMap
                .Select(c => Mathf.Pow(c.Value - solution.ChemicalMap[c.Key], 2))
                .Sum() / target.ChemicalMap.Count;
        }

        public static float fitnessHamming(ChemicalProfile solution, float weight = 1)
        {
            ChemicalProfile target = GameManager.Instance.targetProfile;

            float counter = 0;
            
            for (int i = 0; i < target.ChemicalElements.Count; i++)
            {
                counter += Mathf.Abs(target.ChemicalElements[i].amount - solution.ChemicalElements[i].amount) >= 0.02f * weight ? 1 : 0;
            }

            return 1 - counter / target.ChemicalElements.Count;
        }
        
        public static (ChemicalProfile, ChemicalProfile) crossoverX(ChemicalProfile a, ChemicalProfile b)
        {
            int pivot = Random.Range(1, a.ChemicalMap.Count);

            ChemicalElementTuple[] newA = new ChemicalElementTuple[a.ChemicalElements.Count];
            ChemicalElementTuple[] newB = new ChemicalElementTuple[a.ChemicalElements.Count];

            Array.Copy(
                a.ChemicalElements.ToArray(), 
                0,
                newA, 
                0, 
                pivot);
            Array.Copy(
                b.ChemicalElements.ToArray(),
                pivot,
                newA,
                pivot,
                b.ChemicalElements.Count - pivot
                );
            Array.Copy(
                b.ChemicalElements.ToArray(), 
                0,
                newB, 
                0, 
                pivot);
            Array.Copy(
                a.ChemicalElements.ToArray(),
                pivot,
                newB,
                pivot,
                a.ChemicalElements.Count - pivot
            );

            float normalizationSumA = 0;
            float normalizationSumB = 0;
            for (int i = 0; i < newA.Length; i++)
            {
                normalizationSumA += newA[i].amount;
                normalizationSumB += newB[i].amount;
            }

            Dictionary<ChemicalElement, float> crossedProfileA = new Dictionary<ChemicalElement, float>();
            Dictionary<ChemicalElement, float> crossedProfileB = new Dictionary<ChemicalElement, float>();

            for (int i = 0; i < newA.Length; i++)
            {
                crossedProfileA.Add(newA[i]._chemicalElement, newA[i].amount / normalizationSumA);
                crossedProfileB.Add(newB[i]._chemicalElement, newB[i].amount / normalizationSumB);
            }

            return (new ChemicalProfile(crossedProfileA), new ChemicalProfile(crossedProfileB));
        }

        public static IEnumerable<float> normalize(this IEnumerable<float> vector)
        {
            //float norma = Mathf.Sqrt(vector.Sum(f => f * f));
            float sum = vector.Sum();
            return vector.Select(f => sum > 0 ? f / sum : 0.0f);
        }

        public static ChemicalProfile mutation(ChemicalProfile solution, float weight = 1)
        {
            var sum = solution + new ChemicalProfile(new Dictionary<ChemicalElement, float>(), 0.2f * weight);

            Dictionary<ChemicalElement, float> mutatedProfile = new Dictionary<ChemicalElement, float>();

            float normalizationSum = 0;
            for (int i = 0; i < sum.ChemicalElements.Count; i++)
            {
                normalizationSum += sum.ChemicalElements[i].amount;
            }
            
            for (int i = 0; i < sum.ChemicalElements.Count; i++)
            {
                var mutatedValue = sum.ChemicalElements[i].amount / normalizationSum;
                mutatedProfile.Add(sum.ChemicalElements[i]._chemicalElement, mutatedValue);
            }

            return new ChemicalProfile(mutatedProfile);
        }

        private static void swap(ref ChemicalProfile a, ref ChemicalProfile b, ChemicalElement c, ChemicalElement d)
        {
            var tmp = a.ChemicalMap[c];
            a.ChemicalMap[c] = b.ChemicalMap[d];
            b.ChemicalMap[d] = tmp;
        }
    }
}