﻿using UnityEngine;

public interface IHasColor
{
    Color getColor();
}