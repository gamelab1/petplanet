using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GradientUtils
{
    
    public delegate bool MaterialTransformerOverTime(Material material, Color[] colors);
    
    public static Color[] fromColorToPalette(Color baseColor)
    {
        var (hue, s, v) = (0.0f, 0.0f, 0.0f);

        Color.RGBToHSV(baseColor, out hue, out s, out v);

        return new[]
        {
            Color.HSVToRGB(hue, s, v),
            Color.HSVToRGB(hue, Mathf.Clamp(s + Random.Range(-0.1f, -0.2f), 0, 1),
                Mathf.Clamp(v + Random.Range(0.1f, 0.2f), 0, 1)),
            Color.HSVToRGB(hue, Mathf.Clamp(s + Random.Range(-0.1f, -0.2f), 0, 1),
                Mathf.Clamp(v + Random.Range(-0.1f, -0.2f), 0, 1))
        };
    }

    public static Color blendTo(Color from, Color to, float t)
    {
        return Color.Lerp(from, to, t);
    }

    public static bool applyTransform(Material material, Color[] colors, GradientTarget gradientTarget)
    {
        GradientTransformerInfo gradientTransformerInfo = GradientManager.DefaultTransformer[gradientTarget].transformerInfo;
        
        GradientManager.DefaultTransformer[gradientTarget].transformerInfo.addTime(Time.fixedDeltaTime);
        float step = Mathf.Min(
            GradientManager.DefaultTransformer[gradientTarget].transformerInfo._elapsedTime / gradientTransformerInfo._time, 
            1.0f
            );

        for (int i = 0; i < colors.Length; i++)
        {
            var partial = blendTo(
                GameManager.Instance.playerPalettes[gradientTarget][i], 
                colors[i], 
                step
            );
            material.SetColor(Shader.PropertyToID(gradientTarget.shaderReferenceStrings()[i]), partial);
        }
        
        if (GradientManager.DefaultTransformer[gradientTarget].transformerInfo._elapsedTime >= gradientTransformerInfo._time)
        {
            GradientManager.DefaultTransformer[gradientTarget].transformerInfo
                .addTime(-GradientManager.DefaultTransformer[gradientTarget].transformerInfo._elapsedTime);
            for (int i = 0; i < colors.Length; i++)
            {
                GameManager.Instance.playerPalettes[gradientTarget][i] = colors[i];
            }

            return false;
        }

        return true;
    }
    
    public class GradientTransformerInfo
    {
        public Queue<Color[]> _queue;
        public Coroutine _coroutine;
        public float _time;
        public float _elapsedTime;

        public GradientTransformerInfo(Queue<Color[]> queue, Coroutine coroutine, float time)
        {
            _queue = queue;
            _coroutine = coroutine;
            _time = time;
            _elapsedTime = 0.0f;
        }

        public void addTime(float time)
        {
            this._elapsedTime += time;
        }

        public void setCoroutine(Coroutine coroutine)
        {
            this._coroutine = coroutine;
        }
    }

    public class GradientTransformerHandler
    {
        public MaterialTransformerOverTime gradientMaterialTransformerProcessor;
        public Func<ChemicalProfile, Color[]> fromProfileToPalette;
        public GradientTransformerInfo transformerInfo;

        public GradientTransformerHandler(MaterialTransformerOverTime gradientMaterialTransformerProcessor,
            Func<ChemicalProfile, Color[]> fromProfileToPalette,
            GradientTransformerInfo transformerInfo)
        {
            this.gradientMaterialTransformerProcessor = gradientMaterialTransformerProcessor;
            this.transformerInfo = transformerInfo;
            this.fromProfileToPalette = fromProfileToPalette;
        }
    }
}