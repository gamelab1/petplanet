using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace Utilities
{
    public static class Utils<T>
    {
        public static IEnumerable<T> ToIEnumerable(IEnumerator enumerator)
        {
            while (enumerator.MoveNext())
            {
                yield return (T) enumerator.Current;
            }
        }

        public static T[] shift(T[] toShift, int step = 1, bool right = false)
        {
            var tmp = new T[toShift.Length];

            //Copy the content of the first part of the content into the new array 
            Array.Copy(
                toShift,
                right ? 0 : step,
                tmp,
                right ? step : 0,
                toShift.Length - step);

            //Copy the leftover (step) elements into the new array 
            Array.Copy(
                toShift,
                right ? toShift.Length - step : 0,
                tmp,
                right ? 0 : toShift.Length - step,
                step);
            return tmp;
        }

        public static T[] shuffle(T[] array)
        {
            Random rng = new Random();
            var res = array;

            for (int i = 0; i < res.Count() - 2; i++)
            {
                var ij = (i, rng.Next(i, res.Count()));
                T listI = res[ij.i];
                res[ij.i] = res[ij.Item2];
                res[ij.Item2] = listI;
            }

            return res;
        }

        public static T blendQuantity(T from, T to, float t, Func<T, float> extractor = null, Func<float, T> composer = null)
        {
            if (typeof(T) == typeof(Color))
            {
                return new[] {Color.Lerp(new[] {from}.Cast<Color>().First(), new[] {to}.Cast<Color>().First(), t)}
                    .Cast<T>().First();
            }

            var partialQuantity = Mathf.Lerp(
                extractor != null ? extractor.Invoke(from) : new[] {from}.Cast<float>().First(), 
                extractor != null ? extractor.Invoke(to) : new[] {to}.Cast<float>().First(), 
                t);
            return composer != null ? composer.Invoke(partialQuantity) : new[] {partialQuantity}.Cast<T>().First();
        }
    }

    public static class Utils
    {
        public static bool isPressing = false;

        public static int numberOfDebugRays = 0;

        public static bool isMobile()
        {
            return Application.platform == RuntimePlatform.Android ||
                   Application.platform == RuntimePlatform.IPhonePlayer;
        }

        public static float getPinchOnScreen()
        {
            if (isMobile())
            {
                if (Input.touchCount == 2)
                {
                    var deltas = (Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition,
                        Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition);

                    var currents = (Input.GetTouch(0).position, Input.GetTouch(1).position);

                    var magnitudes = ((deltas.Item1 - deltas.Item2).magnitude,
                        (currents.Item1 - currents.Item2).magnitude);

                    return (magnitudes.Item2 - magnitudes.Item1) * Constants.PINCH_MOBILE_MULTIPLIER;
                }

                return 0.0f;
            }
            else
            {
                return Input.GetAxis("Mouse ScrollWheel");
            }
        }

        public static MouseMovement getPositionOnScreen(MouseMovement oldPosition)
        {
            if (!isPressing)
            {
                oldPosition.Distance = Vector2.zero;
                oldPosition.Direction = Input.mousePosition;
            }

            if (isMobile())
            {
                if (Input.touchCount == 1)
                {
                    Touch touch = Input.GetTouch(0);

                    if (touch.phase == TouchPhase.Began)
                    {
                        isPressing = true;
                        oldPosition.CurrentPosition = touch.position;
                        oldPosition.StartPosition = touch.position;
                        oldPosition.EndPosition = touch.position;
                        oldPosition.Direction = touch.position;
                        oldPosition.Distance = Vector2.zero;
                        oldPosition.StartTime = Time.time;

                        EventManager.RaiseTouchBeganEvent(oldPosition);
                        
                        return oldPosition;
                    }

                    if (touch.phase == TouchPhase.Moved && isPressing)
                    {
                        //Raise move Move event
                        oldPosition.CurrentPosition = touch.position;
                        EventManager.RaiseTouchMovedEvent(oldPosition);
                    }

                    if (touch.phase == TouchPhase.Ended && isPressing)
                    {
                        isPressing = false;
                        oldPosition.CurrentPosition = touch.position;
                        oldPosition.EndPosition = touch.position;
                        oldPosition.Direction = oldPosition.CurrentPosition - oldPosition.StartPosition;
                        oldPosition.Distance = new Vector2(
                            oldPosition.CurrentPosition.x - oldPosition.StartPosition.x,
                            oldPosition.CurrentPosition.y - oldPosition.StartPosition.y
                        );
                        oldPosition.EndTime = Time.time;

                        EventManager.RaiseToucePressureEvent(oldPosition);
                        EventManager.RaiseTouchEndedEvent(oldPosition);

                        return oldPosition;
                    }
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    isPressing = true;
                    oldPosition.CurrentPosition = Input.mousePosition;
                    oldPosition.StartPosition = Input.mousePosition;
                    oldPosition.EndPosition = Input.mousePosition;
                    oldPosition.Direction = Input.mousePosition;
                    oldPosition.Distance = Vector2.zero;
                    oldPosition.StartTime = Time.time;

                    EventManager.RaiseTouchBeganEvent(oldPosition);
                    
                    return oldPosition;
                }

                if (Input.GetMouseButton(0))
                {
                    //Raise move Move event
                    oldPosition.CurrentPosition = Input.mousePosition;
                    EventManager.RaiseTouchMovedEvent(oldPosition);
                }

                if (Input.GetMouseButtonUp(0))
                {
                    isPressing = false;
                    oldPosition.CurrentPosition = Input.mousePosition;
                    oldPosition.EndPosition = Input.mousePosition;
                    oldPosition.Direction = oldPosition.CurrentPosition - oldPosition.StartPosition;
                    oldPosition.Distance = new Vector2(
                        oldPosition.CurrentPosition.x - oldPosition.StartPosition.x,
                        oldPosition.CurrentPosition.y - oldPosition.StartPosition.y
                    );
                    oldPosition.EndTime = Time.time;

                    EventManager.RaiseToucePressureEvent(oldPosition);
                    EventManager.RaiseTouchEndedEvent(oldPosition);
                    return oldPosition;
                }
            }

            return oldPosition;
        }


        public static Vector2 getForceG(Rigidbody2D r1, Rigidbody2D r2, Color debugColor = new Color())
        {
            var distance = r2.position - r1.position;

            return distance.magnitude == 0
                ? Vector2.zero
                : (r2.position - r1.position).normalized *
                  (Constants.GRAVITATIONAL_CONSTANT * r1.mass * r2.mass *
                   (1 / Mathf.Pow((r1.position - r2.position).magnitude, 2)));
        }

        public static Vector2 getSpawnPosition(float radiusThreshold, float angleThreshold = 0)
        {
            var radius = radiusThreshold + UnityEngine.Random.value * 10;
            var angle = Mathf.Max(angleThreshold, 2 * Mathf.PI * UnityEngine.Random.value);

            return new Vector2(
                radius * Mathf.Cos(angle),
                radius * Mathf.Sin(angle)
            );
        }

        public static GameObjectProfile toProfile(GameObject g)
        {
            var planet = g.GetComponent<Planet>();
            var planetSurfaceEffects = g.GetComponent<PlanetSurfaceEffects>();
            var rigidbody2D = g.GetComponent<Rigidbody2D>();

            if (planet && planetSurfaceEffects)
            {
                //Collect milestones
                var milestonesIter = GameManager.Instance.milestones.Values.GetEnumerator();
                List<(MilestoneType, int)> milestones = new List<(MilestoneType, int)>();
                
                for (; milestonesIter.MoveNext();)
                {
                    milestones.Add((milestonesIter.Current.MilestoneType, milestonesIter.Current.Level));
                }

                return new GameObjectProfile(
                    g.GetInstanceID(),
                    g.transform.position,
                    rigidbody2D.velocity,
                    rigidbody2D.mass,
                    g.transform.localScale,
                    g.GetComponent<Renderer>().material,
                    g.activeSelf,
                    g.GetComponent<Collider2D>().bounds,
                    planet.extraMass,
                    planet.chemicalProfile,
                    planetSurfaceEffects.m_GlobalHits,
                    milestones
                );
            }

            return new GameObjectProfile(
                g.GetInstanceID(),
                g.transform.position,
                rigidbody2D.velocity,
                rigidbody2D.mass,
                g.transform.localScale,
                g.GetComponent<Renderer>().material,
                g.activeSelf,
                g.GetComponent<Collider2D>().bounds
            );
        }

        public static bool collidesWith(GameObjectProfile body1, GameObjectProfile body2, float inTime)
        {
            //Pf = V * (Tf) + Pi
            //Y = Va * (X) + Sa
            //Y = Ve * (X) + Se
            //0 = Va * X - Ve * X + Sa - Se
            // (Se - Sa) / (Va - Ve) = X

            // |--------0--------|

            if (Mathf.Approximately(
                Vector2.Dot(body1.Velocity, body2.Velocity) * 1e6f,
                body1.Velocity.magnitude * body2.Velocity.magnitude * 1e6f
            ))
            {
                return false;
            }

            return new[]
                {
                    body2.Bounds.center,
                    body2.Bounds.max,
                    body2.Bounds.min
                }.Select(p =>
                {
                    var Tx = (p.x - body1.Position.x) / (body1.Velocity.x - body2.Velocity.x);
                    var Ty = (p.y - body1.Position.y) / (body1.Velocity.y - body2.Velocity.y);

                    Debug.Log($"({body1.ID},{body2.ID}) -> {Tx} {Ty} / {inTime}");

                    return (Tx, Ty);
                })
                .Any(time => Mathf.Approximately(time.Tx, time.Ty) && time.Tx <= inTime);
        }

        public static Coroutine disableFor(Selectable selectable, float time, MonoBehaviour coroutineHolder)
        {
            return coroutineHolder.StartCoroutine(disableElementFor(selectable, time));
        }

        static IEnumerator disableElementFor(Selectable selectable, float time)
        {
            selectable.interactable = false;
            yield return new WaitForSecondsRealtime(time);
            selectable.interactable = true;
        }

        public static void substituteKeyValue<T, V>(Dictionary<T, V> dictionary, T key, V newValue, Action<V> supplier)
        {
            if (dictionary.ContainsKey(key))
            {
                supplier(dictionary[key]);
                dictionary.Remove(key);
            }

            dictionary.Add(key, newValue);
        }

        public static float remap(float value, (float, float) oldBoundaries, (float, float) newBoundaries)
        {
            if (oldBoundaries.Item2 - oldBoundaries.Item1 <= 0)
            {
                Debug.Log("Che cazzo fai?");
                throw new Exception("Che cazzo fai?");
            }

            //low2 + (value - low1) * (high2 - low2) / (high1 - low1)

            return newBoundaries.Item1 + (value - oldBoundaries.Item1) * (newBoundaries.Item2 - newBoundaries.Item1) /
                (oldBoundaries.Item2 - oldBoundaries.Item1);
        }

        public static Transform getNearestTransformFrom(Vector3 position, Transform group)
        {
            return Utils<Transform>.ToIEnumerable(group.GetEnumerator())
                .Select(t => (t, (t.position - position).sqrMagnitude))
                .OrderBy(tuple => tuple.sqrMagnitude)
                .First().t;
        }

        public static IEnumerable<T> shuffle<T>(this IEnumerable<T> list)
        {
            Random rng = new Random();
            var res = list.ToArray();

            for (int i = 0; i < res.Count() - 2; i++)
            {
                var ij = (i, rng.Next(i, res.Count()));
                T listI = res[ij.i];
                res[ij.i] = res[ij.Item2];
                res[ij.Item2] = listI;
            }

            return res;
        }
    }
}