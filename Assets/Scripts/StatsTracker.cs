﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class StatsTracker : MonoBehaviour
{
    [SerializeField] private Text _fpsText;
    [SerializeField] private Text _displayInfoText;
    [SerializeField] private Text _debugInfoText;
    [SerializeField] private float _hudRefreshRate = 1f;

    void OnEnable()
    {
        Application.logMessageReceived += Log;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= Log;
    }

    public void Log(string logString, string stackTrace, LogType type)
    {
        if (logString.StartsWith("INPUT") || type == LogType.Error || type == LogType.Exception)
        {
            _debugInfoText.text = logString;
        }
    }

    private void Start()
    {
        List<Text> texts = GetComponentsInChildren<Text>().ToList();
        _fpsText = texts[1];
        _debugInfoText = texts[2];
        _displayInfoText = texts[0];

        _displayInfoText.text = string.Format("Display: {0}x{1} / {2}x{3} - {4}Hz",
            Screen.currentResolution.width, Screen.currentResolution.height,
            Camera.main.pixelWidth, Camera.main.pixelHeight,
            Screen.currentResolution.refreshRate);
    }


    private float _timer;

    private void Update()
    {
        if (Time.unscaledTime > _timer)
        {
            int fps = (int) (1f / Time.unscaledDeltaTime);
            _fpsText.text = "FPS: " + fps;
            _timer = Time.unscaledTime + _hudRefreshRate;
        }
    }
}