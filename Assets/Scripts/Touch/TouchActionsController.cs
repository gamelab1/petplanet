﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class TouchActionsController : MonoBehaviour
{
    public bool canShake = true;

    public Dictionary<TouchActions, Coroutine> coroutines = new Dictionary<TouchActions, Coroutine>();

    public GameObject star;
    public float cameraMovementTime;

    public GameObject menu;
    
    public void Shake(Button b)
    {
        if (!canShake)
        {
            return;
        }

        GameManager.Instance.player.GetComponent<Animator>().SetTrigger("Shake");

        //Trigger VFX on to get chromatic aberration on
        EventManager.RaiseToggleVolumetricEffectEvent(true, Constants.CHROMATIC_ABERRATION_NAME, 1.0f);
        
        Utils.substituteKeyValue(
            coroutines,
            TouchActions.Shake,
            Utilities.Utils.disableFor(
                b,
                GameManager.Instance.globalCoolDown,
                this
            ),
            this.StopCoroutine
        );
    }

    public void MoveCameraTo(Button b)
    {
        CameraMovement cameraMovement = Camera.main.GetComponent<CameraMovement>();

        GameManager.Instance.cameraTarget.MoveNext();
        Transform targetTransform = GameManager.Instance.cameraTarget.Current.Item1;
        
        cameraMovement.StartCoroutine(cameraMovement.moveToPosition(
            targetTransform,
            targetTransform.GetComponent<CircleCollider2D>(), 
            cameraMovementTime)
        );

        Utils.substituteKeyValue(
            coroutines,
            TouchActions.Sun,
            Utilities.Utils.disableFor(
                b,
                GameManager.Instance.globalCoolDown,
                this
            ),
            this.StopCoroutine
        );
        
    }

    public void OpenMenu(Button b)
    {
        menu.SetActive(!menu.activeSelf);

        GameManager.Instance.isMenuInFront = menu.activeSelf;
        
        Utils.substituteKeyValue(
            coroutines,
            TouchActions.Menu,
            Utils.disableFor(
                b,
                GameManager.Instance.globalCoolDown,
                this
                ),
            this.StopCoroutine
            );
    }

}