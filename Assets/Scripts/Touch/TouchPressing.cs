using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TouchPressing : MonoBehaviour, IUseFavor
{
    public MouseMovement Movement = new MouseMovement();
    public GameObject inpulsePrefab;
    public Transform inpulseObjectsPool;

    public float sensitivity = 0.2f;
    public float panThreshold = 0.2f;

    public float timeToLive = 1.0f;

    [SerializeField] private List<GameObject> inpulseObjects = new List<GameObject>();

    private IEnumerator<GameObject> inpulseObjectsGenerator;
    
    private void Start()
    {
         inpulseObjects = Utilities.Utils<Transform>.ToIEnumerable(inpulseObjectsPool.GetEnumerator())
                    .Select(t => t.gameObject)
                    .ToList();
         
         inpulseObjectsGenerator = getNextAvailableInpulseObject();
    }

    private void OnEnable()
    {
        EventManager.touchPressureEvent += touchPressure;
    }

    private void OnDisable()
    {
        EventManager.touchPressureEvent -= touchPressure;
    }


    public void touchPressure(MouseMovement movement)
    {
        if (checkGlobalConditions() || !isInsideActiveArea(movement.CurrentPosition))
        {
            return;
        }

        Movement = movement;

        var positionOnScreen = new Vector3(Movement.CurrentPosition.x, Movement.CurrentPosition.y, 1.0f);
        
        // Debug.Log(string.Format("Getting the event {0} {1} position {2}", Movement.Duration, Movement.Distance.sqrMagnitude, positionOnScreen));
        
        if (Movement.StartTime != 0.0f 
            && Movement.Duration > sensitivity 
            && Movement.Distance.sqrMagnitude <= panThreshold * panThreshold
            && hasEnoughFavorToRun()
            )
        {
            //Spawn a inpulse object
            Vector3 posOnScreen = Camera.main.ScreenToWorldPoint(positionOnScreen);
            Vector3 posOnViewPort = Camera.main.ScreenToViewportPoint(positionOnScreen);
            
            inpulseObjectsGenerator.MoveNext();
            var inpulse = inpulseObjectsGenerator.Current;

            inpulse.transform.position = posOnScreen;
            inpulse.SetActive(true);

            //Use resources
            GameManager.Instance.updateFavor(-getNecessaryFavor());
            
            //Trigger VFX on to get chromatic aberration on
            EventManager.RaiseToggleVolumetricEffectEvent(true, Constants.CHROMATIC_ABERRATION_NAME, 1.0f);
            
            StartCoroutine(hide(inpulse));
        }
    }
    
    public IEnumerator hide(GameObject g)
    {
        yield return new WaitForSeconds(timeToLive);
        
        //Trigger VFX on to get chromatic aberration off
        EventManager.RaiseToggleVolumetricEffectEvent(false, Constants.CHROMATIC_ABERRATION_NAME, 1.0f);
        
        g.SetActive(false);
    }

    private bool isInsideActiveArea(Vector2 positionOnScreen)
    {
        var cameraHeight = Camera.main.pixelHeight;

        return positionOnScreen.y >= cameraHeight * GameManager.Instance.activeArea.x 
               && positionOnScreen.y <= cameraHeight * GameManager.Instance.activeArea.y;
    }
    
    private bool checkGlobalConditions()
    {
        return GameManager.Instance.isCameraMoving || GameManager.Instance.isMenuInFront;
    }
    
    private IEnumerator<GameObject> getNextAvailableInpulseObject()
    {
        while (true)
        {
            var selected = inpulseObjects.FirstOrDefault(g => !g.activeSelf);
            
            if (selected == null)
            {
                var newly = GameObject.Instantiate(inpulsePrefab, Vector3.zero, Quaternion.identity,
                    inpulseObjectsPool);
                newly.SetActive(false);
                inpulseObjects.Add(newly);
                selected = newly;
            }

            yield return selected;
        }
    }

    public float getNecessaryFavor()
    {
        return GameManager.Instance.maxFavor / 2;
    }

    public bool hasEnoughFavorToRun()
    {
        return getNecessaryFavor() <= GameManager.Instance.actualFavor;
    }
}