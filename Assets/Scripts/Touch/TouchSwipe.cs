﻿using UnityEngine;
using Utilities;

public partial class TouchSwipe : MonoBehaviour, IUseFavor
{
    public Vector3 positionOnScreen = Vector3.zero;
    public MouseMovement Movement = new MouseMovement();
    public float minimumSwipe = 30f;

    private void Start()
    {
        minimumSwipe = Camera.main.pixelWidth / 3.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (checkGlobalConditions())
        {
            return;
        }
        
        //Get the pointer status
        Movement = Utilities.Utils.getPositionOnScreen(Movement);
        positionOnScreen = Movement.CurrentPosition;


        DebugSwipe();

        //If swipe magnitude is bigger than zero
        if (Movement.Distance.x != 0 && !GameManager.Instance.cannotReverse && hasEnoughFavorToRun() || Movement.Distance.x > 0)
        {
            if (Mathf.Abs(Movement.Distance.x) > minimumSwipe)
            {
                var distanceClamped = Mathf.Min(Mathf.Abs(Movement.Distance.x), Camera.main.pixelWidth);
                var weightedDistance = distanceClamped / Camera.main.pixelWidth;

                var targetRotationSpeed =
                    GameManager.Instance.timeMachine.getTargetRotationSpeed(weightedDistance, Movement.Distance.x);

                //Accelerate the sphere by the swipe amount
                GameManager.Instance.timeMachine.StartCoroutine(GameManager.Instance.timeMachine.accelerateFor(0f, targetRotationSpeed));
                if (GameManager.Instance.currentTimeMachineJob != null)
                {
                    GameManager.Instance.timeMachine.StopCoroutine(GameManager.Instance.currentTimeMachineJob);
                }
                
                //Use resource
                GameManager.Instance.updateFavor(-getNecessaryFavor());

                //Start another coroutine to stop the acceleration.
                GameManager.Instance.currentTimeMachineJob = GameManager.Instance.timeMachine.StartCoroutine(
                    GameManager.Instance.timeMachine.accelerateFor(
                        weightedDistance * GameManager.Instance.timeMachine.getMaxDelayTimeRestore(),
                        GameManager.Instance.timeMachine.getBaseRotationSpeed(),
                        targetRotationSpeed > 0
                    )
                );
            }
        }
    }

    private bool checkGlobalConditions()
    {
        return GameManager.Instance.isCameraMoving || GameManager.Instance.isCameraZooming || GameManager.Instance.isMenuInFront;
    }

    private void DebugSwipe()
    {
        if (Movement.Distance.x != 0)
        {
            //Debug.Log($"Distance from last release {Movement.Distance}");
            Debug.DrawLine(
                Camera.main.ScreenToWorldPoint(Movement.StartPosition),
                Camera.main.ScreenToWorldPoint(Movement.EndPosition),
                Movement.Distance.x >= 0 ? Color.blue : Color.red,
                1f
            );

            if (Utils.isMobile())
            {
                Debug.Log(string.Format("INPUT: {0}", Movement.Distance.x));
            }
            // var distanceClamped = Mathf.Min(Mathf.Abs(Movement.Distance), Camera.main.pixelWidth);
            // var weightedDistance = distanceClamped / Camera.main.pixelWidth * 100;
            // Debug.Log($"Weighted distance {weightedDistance}, screen dimension = {Camera.main.pixelWidth}");
        }
    }

    public float getNecessaryFavor()
    {
        return 1000.0f;
    }

    public bool hasEnoughFavorToRun()
    {
        return getNecessaryFavor() >= GameManager.Instance.actualFavor;
    }
}