using UnityEngine;

public class TouchPinch : MonoBehaviour
{
    [SerializeField] private float minimumPinch = 30f;
    private void Start()
    {
        minimumPinch = Camera.main.pixelWidth / 4 * Constants.PINCH_MOBILE_MULTIPLIER;
    }

    private void Update()
    {
        if (checkGlobalConditions())
        {
            return;
        }
        
        float pinchAmount = Utilities.Utils.getPinchOnScreen();

        EventManager.RaisePinchEvent(pinchAmount);
    }
    
    private bool checkGlobalConditions()
    {
        return GameManager.Instance.isCameraMoving || GameManager.Instance.isMenuInFront;
    }
}