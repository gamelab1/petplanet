using UnityEngine;

public class MouseMovement
{
    private Vector2 currentPosition;
    private Vector2 distance;
    private Vector2 startPosition;
    private Vector2 endPosition;
    private Vector2 direction;
    private float startTime;
    private float endTime;

    public float Duration
    {
        get => endTime - startTime;
    }
    
    public Vector2 CurrentPosition
    {
        get => currentPosition;
        set => currentPosition = value;
    }

    public Vector2 Distance
    {
        get => distance;
        set => distance = value;
    }

    public Vector2 StartPosition
    {
        get => startPosition;
        set => startPosition = value;
    }

    public Vector2 EndPosition
    {
        get => endPosition;
        set => endPosition = value;
    }

    public Vector2 Direction
    {
        get => direction;
        set => direction = value;
    }

    public float StartTime
    {
        get => startTime;
        set => startTime = value;
    }

    public float EndTime
    {
        get => endTime;
        set => endTime = value;
    }
}