using System;
using UnityEngine;

public class EventManager
{
    //ChangeMainProfile event
    public delegate void ChangeColorHandler(GradientTarget target, ChemicalProfile newProfile);
    public static event ChangeColorHandler changeColorEvent;

    //PinchToZoom event
    public delegate void PinchHandler(float amount);
    public static event PinchHandler pinchEvent;
    
    //Deteriorate event
    public delegate void DeteriorateHandler(float mass, ChemicalProfile profile);

    public static event DeteriorateHandler deteriorateEvent;
    
    //CollisionEvent
    public delegate void IncomingCollisionHandler(Vector2 position, ChemicalProfile profile, float mass);

    public static event IncomingCollisionHandler incomingCollisionEvent;

    //TouchPressureEvent
    public delegate void TouchPressureHandler(MouseMovement movement);
    public static event TouchPressureHandler touchPressureEvent;

    //FavorUsageEvent
    public delegate void FavorUsageHandler(float amout);
    public static event FavorUsageHandler favorUsageEvent;
    
    //ToggleVolumetricEffect
    public delegate void ToggleVolumetricEffect(bool active, String effect, float amount);
    public static event ToggleVolumetricEffect toggleVolumetricEffectEvent;
    
    //ExtraMassChangeEvent
    public delegate void ExtraMassChangeHandler(ChemicalProfile profile);

    public static event ExtraMassChangeHandler extraMassChangeEvent;

    public delegate void MilestoneCheck(MilestoneType milestoneType, Func<Boolean> predicate);
    public static event MilestoneCheck milestoneCheckEvent;

    public delegate void MilestoneLevelUp(MilestoneType milestoneType, int level);
    public static event MilestoneLevelUp milestoneLevelUpEvent;

    public delegate void TouchBegan(MouseMovement movement);

    public static event TouchBegan touchBeganEvent;
    
    public delegate void TouchMoved(MouseMovement movement);

    public static event TouchMoved touchMovedEvent;
    
    public delegate void TouchEnded(MouseMovement movement);
    public static event TouchEnded touchEndedEvent;
    
    public delegate void ConstellationGameHandler(int constellationIndex);

    public static ConstellationGameHandler constellationGameStartEvent;

    public static ConstellationGameHandler constellationGameEndEvent;
    public delegate void UICreateCommand<T>(String text, float ttl, Action<T> callback, T target);

    public static UICreateCommand<String> UICreateCommandEvent;

    public delegate void EnqueueToDestroy(GameObject target, Action<GameObject> callback, float delay);

    public static EnqueueToDestroy enqueueToDestroyEvent;


    public static void RaiseEnqueueGameObjectToDestroy(GameObject target, Action<GameObject> callback, float delay)
    {
        enqueueToDestroyEvent?.Invoke(target, callback, delay);
    }


    public static void RaiseUICreateCommandEvent<T>(String text, float ttl, Action<T> callback, T target)
    {
        UICreateCommandEvent?.Invoke(text, ttl, callback as Action<String>, target as String);
    }
    

    public static void RaiseConstellationGameStartEvent(int constellationIndex)
    {
        constellationGameStartEvent?.Invoke(constellationIndex);
    }
    
    public static void RaiseConstellationGameEndEvent(int constellationIndex)
    {
        constellationGameEndEvent?.Invoke(constellationIndex);
    }
    
    public static void RaiseTouchEndedEvent(MouseMovement movement)
    {
        touchEndedEvent?.Invoke(movement);
    }
    
    public static void RaiseTouchMovedEvent(MouseMovement movement)
    {
        touchMovedEvent?.Invoke(movement);
    }
    
    public static void RaiseTouchBeganEvent(MouseMovement movement)
    {
        touchBeganEvent?.Invoke(movement);
    }

    public static void RaiseMilestoneLevelUpEvent(MilestoneType milestoneType, int level)
    {
        milestoneLevelUpEvent?.Invoke(milestoneType, level);
    }

    public static void RaiseMilestoneCheckEvent(MilestoneType milestoneType, Func<Boolean> predicate)
    {
        milestoneCheckEvent?.Invoke(milestoneType, predicate);
    }

    public static void RaiseExtraMassChangeEvent(ChemicalProfile profile)
    {
        extraMassChangeEvent?.Invoke(profile);
    }

    public static void RaiseToggleVolumetricEffectEvent(bool active, String effect, float action)
    {
        toggleVolumetricEffectEvent?.Invoke(active, effect, action);
    }

    public static void RaiseFavorUsageEvent(float amount)
    {
        favorUsageEvent?.Invoke(amount);
    }
    
    
    public static void RaiseToucePressureEvent(MouseMovement movement)
    {
        touchPressureEvent?.Invoke(movement);
    }
    
    public static void RaiseIncomingCollisionEvent(Vector2 position, ChemicalProfile profile, float mass)
    {
        incomingCollisionEvent?.Invoke(position, profile, mass);
    }

    public static void RaiseChangeColorEvent(GradientTarget target, ChemicalProfile newProfile)
    {
        changeColorEvent?.Invoke(target, newProfile);
    }

    public static void RaisePinchEvent(float amount)
    {
        pinchEvent?.Invoke(amount);
    }

    public static void RaiseDeteriorateEvent(ExtraMass extraMass)
    {
        deteriorateEvent?.Invoke(extraMass.Mass, extraMass.ChemicalProfile);
    }
    
}