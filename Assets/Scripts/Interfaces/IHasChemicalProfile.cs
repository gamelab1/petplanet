﻿
public interface IHasChemicalProfile
{
    ChemicalProfile getChemicalProfile();

    void setChemicalProfile(ChemicalProfile cp);
}
