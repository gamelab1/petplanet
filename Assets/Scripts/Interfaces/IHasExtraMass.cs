public interface IHasExtraMass
{
    ExtraMass getExtraMass();
}