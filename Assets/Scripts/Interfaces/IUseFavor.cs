public interface IUseFavor
{
    float getNecessaryFavor();

    bool hasEnoughFavorToRun();
}