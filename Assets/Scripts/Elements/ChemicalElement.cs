using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public enum ChemicalElement
{
    Ag = 47,    //Argento
    Au = 79,    //Oro
    C = 6,      //Carbonio
    Cl = 17,    //Cloro
    Cu = 29,    //Rame
    H = 1,      //Idrogeno
    He = 19,    //Helium
    Ne = 10,    //Neon
    O = 8,      //Ossigeno
    P = 15,     //Fosforo
    Pu = 94,    //Plutonio
    S = 16,     //Zolfo
    U = 92,     //Uranio
    
}

public static class ChemicalElementHandler
{
    public static ChemicalElement[] chemicalElements =
        Enum.GetValues(typeof(ChemicalElement)).Cast<ChemicalElement>().ToArray();
    
    private static Dictionary<ChemicalElement, (Color, Func<Color, Vector3>)> chemicalElementColorMap =
        new Dictionary<ChemicalElement,
            (Color, Func<Color, Vector3>)>()
        {
            {ChemicalElement.Pu, (new Color(0 / 255f,255 / 255f,0 / 255f), toTransformedHSVColor)},
            {ChemicalElement.P, (new Color(255 / 255f,36 / 255f,0 / 255f), toTransformedHSVColor)},
            {ChemicalElement.Cl, (new Color(223 / 255f, 252 / 255f, 3 / 255f), toTransformedHSVColor)},
            {ChemicalElement.U, (new Color(192 / 255f,255 / 255f,0 / 255f), toTransformedHSVColor)},
            {ChemicalElement.O, (new Color(135 / 255f,206 / 255f,250 / 255f), toTransformedHSVColor)},
            {ChemicalElement.S, (new Color(234 / 255f, 10 / 255f,139 / 255f), toTransformedHSVColor)},
            {ChemicalElement.Cu, (new Color(247 / 255f,127 / 255f,66 / 255f), toTransformedHSVColor)},
            {ChemicalElement.Ne, (new Color(255 / 255f,100 / 255f,0 / 255f), toTransformedHSVColor)},
            {ChemicalElement.H, (new Color(161 / 255f ,141 / 255f,219 / 255f), toTransformedHSVColor)},
            {ChemicalElement.Au, (new Color(212 / 255f, 175 / 255f, 55 / 255f), toTransformedHSVColor)},
            {ChemicalElement.Ag, (new Color(196 / 255f, 202 / 255f, 206 / 255f), 
                    (c) =>
                    {
                        float h, s, v;
                        Color.RGBToHSV(c, out h, out s, out v);
                        return new Vector3(200f / 255f, s, v + Random.Range(-0.025f, 0.025f));
                    }
                )},
            {ChemicalElement.He, (new Color(102 / 255f, 0 / 255f, 161 / 255f), toTransformedHSVColor)},
            {ChemicalElement.C, (new Color(84f / 255f, 85f / 255f, 84f / 255f), (c) => 
                    {
                        float h, s, v;
                        Color.RGBToHSV(c, out h, out s, out v);
                        return new Vector3(h, s, v + Random.Range(-0.025f, 0.025f));
                    }
                )
            },
        };

    public static Vector3 toTransformedHSVColor(Color color)
    {
        float h, s, v;
        Color.RGBToHSV(color, out h, out s, out v);
        s += Random.Range(-0.125f, 0.125f);
        v += Random.Range(-0.025f, 0.025f);

        return new Vector3(
            h,
            Mathf.Clamp(s, 0.0f, 1.0f),
            Mathf.Clamp(v, 0.0f, 1.0f)
        );
    }

    public static Color getColor(this ChemicalElement chemicalElement)
    {
        return chemicalElementColorMap[chemicalElement].Item1;
    }

    public static Vector3 getTransformedHSV(this ChemicalElement chemicalElement)
    {
        return chemicalElementColorMap[chemicalElement].Item2.Invoke(chemicalElementColorMap[chemicalElement].Item1);
    }
}