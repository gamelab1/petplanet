﻿using System.Collections;
using System.Linq;
using UnityEngine;
using Utilities;

public class PoolStatus : MonoBehaviour
{
    public int elementCounter;
    public int activeElements;
    public int garbageCollectorTrigger = 150;

    private void Start()
    {
        StartCoroutine(collectTrash());
    }

    private void FixedUpdate()
    {
        elementCounter = transform.childCount;
        activeElements = Utils<Transform>.ToIEnumerable(transform.GetEnumerator()).Count(t => t.gameObject.activeSelf);
    }

    IEnumerator collectTrash()
    {
        while (true)
        {
            yield return new WaitUntil(() => elementCounter >= garbageCollectorTrigger);
        
            Debug.Log($"Collecting trash on {name}");
        
            Utils<Transform>.ToIEnumerable(transform.GetEnumerator())
                .Where(t => !t.gameObject.activeSelf)
                .ToList()
                .ForEach(t =>
                {
                    Destroy(t.gameObject);
                });
        }
    }
    
    
}
