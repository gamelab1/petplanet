﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

public class Asteroid : MonoBehaviour, IHasColor, IHasChemicalProfile
{
    public Animator animator;

    public Color color;

    public float initialTorque = 0.0f;

    public float referenceMass = 7347;
    public float limitMass = 7000;

    public bool needInitialization = true;
    
    public ChemicalProfile chemicalProfile;

    public GeneratorClass generatorClass;

    private Type _generator;
    
    private void Start()
    {
        GetComponent<Rigidbody2D>().AddTorque((initialTorque + (UnityEngine.Random.value - 0.5f) * 3) * 1E3f, ForceMode2D.Force);
        _generator = GeneratorManager._generatorClass[generatorClass];
        
        if (needInitialization)
        {
            //Set mass
            transform.localScale = transform.localScale * (UnityEngine.Random.value + 0.5f);
            GetComponent<Rigidbody2D>().mass = referenceMass * transform.localScale.x;

            chemicalProfile = new ChemicalProfile(new Dictionary<ChemicalElement, float>());
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (!other.gameObject.CompareTag("Player")) //Let's split
        {
            if (other.gameObject.GetComponent<Rigidbody2D>())
            {
                var gameObjects = (this.gameObject, other.gameObject);

                if (other.gameObject.GetComponent<Rigidbody2D>().mass > limitMass  && GetComponent<Rigidbody2D>().mass > limitMass)
                {
                    var fragments = prepareAsteroidFragmentsStructure(gameObjects);

                    GeneratorManager.Instance.handleCollision<AsteroidFragment>(
                        (new List<(Type, GameObject, IEnumerable<AsteroidFragment>)>()
                        {
                            (fragments.Item1.Item1._generator, gameObject, fragments.Item1.Item2),
                            (fragments.Item2.Item1._generator, other.gameObject, fragments.Item2.Item2)
                        }));
                }
            }
        }
        else
        {
            Planet p = other.gameObject.GetComponent<Planet>();
            p.giveExtraMass(new ExtraMass(GetComponent<Rigidbody2D>().mass, chemicalProfile));
            EventManager.RaiseIncomingCollisionEvent(transform.position, chemicalProfile, GetComponent<Rigidbody2D>().mass);
        }
        
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        gameObject.GetComponent<Rigidbody2D>().simulated = false;
            
        //Start the animation
        Utils<Transform>.ToIEnumerable(transform.GetEnumerator()).First().gameObject.SetActive(true);

        StartCoroutine(letMeDie());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Inpulse"))
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            gameObject.GetComponent<Rigidbody2D>().simulated = false;
            
            //Start the animation
            Utils<Transform>.ToIEnumerable(transform.GetEnumerator()).First().gameObject.SetActive(true);

            StartCoroutine(letMeDie());
        }
    }


    private Tuple<Tuple<Asteroid, IEnumerable<AsteroidFragment>>, Tuple<Asteroid, IEnumerable<AsteroidFragment>>> prepareAsteroidFragmentsStructure(
        (GameObject, GameObject) objTuple)
    {
        return (
                (objTuple.Item1.GetComponent<Asteroid>(), getFragments(objTuple.Item1.GetComponent<Asteroid>(), Vector3.right)).ToTuple(),
                (objTuple.Item2.GetComponent<Asteroid>(), getFragments(objTuple.Item2.GetComponent<Asteroid>(), Vector3.up)).ToTuple()
                ).ToTuple();
    }

    private IEnumerable<AsteroidFragment> getFragments(Asteroid g, Vector3 axis)
    {
        var split = getSplitPoints();
        Rigidbody2D rigidbody2D = g.GetComponent<Rigidbody2D>();
        var mass = rigidbody2D.mass;
        Vector3[] positions = {g.transform.position + axis * 2, g.transform.position + axis * (-2)};
        
        return split.Concat(new[] {1.0f - split[0]})
            .Zip(positions,(interval, p) => new AsteroidFragment(
                interval * mass,
                interval * g.transform.localScale.x,
                g.GetComponent<IHasChemicalProfile>().getChemicalProfile() * interval,
                p,
                (p - g.transform.position).normalized * rigidbody2D.velocity.magnitude * interval
            )).ToList();
    }

    private float[] getSplitPoints()
    {
        return new float[] {Mathf.Clamp(0.3f + Random.value, 0.3f, 1) * 0.7f};
    } 
    
    IEnumerator letMeDie()
    {
        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1);
        
        gameObject.SetActive(false);
    }

    public Color getColor()
    {
        return color;
    }

    public ChemicalProfile getChemicalProfile()
    {
        return chemicalProfile;
    }

    public void setChemicalProfile(ChemicalProfile cp)
    {
        this.chemicalProfile = cp;
    }

}
