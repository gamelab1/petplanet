using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

public class Darwin : MonoBehaviour
{
    private List<ChemicalProfile> population = new List<ChemicalProfile>();

    [SerializeField] private int populationLimit = 50;
    
    public float evolutionTick = 3f;

    public ChemicalProfile meanProfile;
    public ChemicalProfile bestProfile;
    public float bestFitnessHamming;
    public float bestFitness;
    public int bestIndex = 0;

    public int generationStep = 0;
    public int generationStepTarget = 100000;
    
    public float crossoverProbability = 0;
    public float mutationProbability = 0;
    public float elitarismProbability = 0;

    public Coroutine evolutionCoroutine;

    private Action<ChemicalProfile> profileSetter;

    private void Start()
    {
        crossoverProbability = crossoverProbability != 0 ? Mathf.Clamp(crossoverProbability, 0, 1) : Random.value;
        mutationProbability = mutationProbability != 0 ? Mathf.Clamp(mutationProbability, 0, 1) : Random.value;
    }

    public void restartEvolution(ChemicalProfile bestProfile)
    {
        if (evolutionCoroutine != null)
        {
            StopCoroutine(evolutionCoroutine);

            if (this.maxHasChanged(this.bestProfile, bestProfile))
            {
                //Debug.Log("Dispatching the event with " + bestProfile.max());
                EventManager.RaiseChangeColorEvent(GradientTarget.Planet, bestProfile);
            }
        }
        generationStep = 0;
        startEvolution(profileSetter);
        population[0] = new ChemicalProfile(bestProfile.ChemicalMap);
        bestProfile = population[0];
        this.profileSetter.Invoke(bestProfile);
    }

    public void startEvolution(Action<ChemicalProfile> profileSetter)
    {
        this.profileSetter = profileSetter;
        population = Enumerable.Range(0, populationLimit)
            .Select(i => new ChemicalProfile(new Dictionary<ChemicalElement, float>()))
            .ToList();
        
        meanProfile = new ChemicalProfile((population
                                               .Skip(1)
                                               .Aggregate(population.ElementAt(0),
                                                   (acc, nextChemicalProfile) => acc + nextChemicalProfile) *
                                           (1.0f / population.Count)).ChemicalElements
            .Select(t => t.amount)
            .normalize()
            .Zip(population.ElementAt(0).ChemicalElements, (f, tuple) => (tuple._chemicalElement, f))
            .ToDictionary(k => k._chemicalElement, v => v.f));

        var bestProfileTuple = population.Zip(Enumerable.Range(0, population.Count), ((profile, i) => (profile, i)) )
            .OrderByDescending(solution => DarwinUtils.fitnessHamming(solution.profile)).First();

        bestProfile = bestProfileTuple.profile;

        bestFitness = DarwinUtils.fitness(bestProfile);
        bestFitnessHamming = DarwinUtils.fitnessHamming(bestProfile);
        bestIndex = bestProfileTuple.i;

        evolutionCoroutine = StartCoroutine(evolutionStep(evolutionTick));
        this.profileSetter.Invoke(bestProfile);
    }

    IEnumerator evolutionStep(float tick)
    {
        while (true)
        {
            yield return new WaitForSeconds(tick);
            //Debug.Log("Let's evolve!");
            evolve();
            
            var bestProfileTuple = population.Zip(Enumerable.Range(0, population.Count), ((profile, i) => (profile, i)) )
                .OrderByDescending(solution => DarwinUtils.fitnessHamming(solution.profile)).First();

            bool maxHasChanged = this.maxHasChanged(bestProfile, bestProfileTuple.profile);
            bestProfile = bestProfileTuple.profile;
            
            bestFitness = DarwinUtils.fitness(bestProfile);
            bestFitnessHamming = DarwinUtils.fitnessHamming(bestProfile, completeness());
            bestIndex = bestProfileTuple.i;
            
            generationStep++;
            
            this.profileSetter.Invoke(bestProfile);
            
            //Se maxHasChanged Raise a change color event.
            if (maxHasChanged)
            {
                //Debug.Log("Dispatching the event with " + bestProfile.max());
                EventManager.RaiseChangeColorEvent(GradientTarget.Planet, bestProfile);
            }
        }
    }

    private void evolve()
    {
        //Selection
        int selectionCount = Random.Range(5, populationLimit);

        List<(ChemicalProfile, int)> selectedSolutions = new List<(ChemicalProfile, int)>();
        // List<(ChemicalProfile, int)> selectedSolutions = Enumerable.Range(0, selectionCount)
        //     .Select(i => Random.Range(0, population.Count))
        //     .Distinct()
        //     .Take(selectionCount)
        //     .Select(i => (population[i], i))
        //     .OrderByDescending(solution => DarwinUtils.fitnessHamming(solution.Item1, completeness()))
        //     .ToList();

        HashSet<int> randomIndeces = new HashSet<int>();
        
        for (int i = 0; i < selectionCount; i++)
        {
            randomIndeces.Add(Random.Range(0, population.Count));
        }

        foreach (var i in randomIndeces)
        {
           selectedSolutions.Add((population[i], i));
        }
        
        selectedSolutions.Sort((a, b) => 
            (int) (DarwinUtils.fitnessHamming(a.Item1, completeness()) - DarwinUtils.fitnessHamming(b.Item1, completeness())));

        selectedSolutions = selectedSolutions
            .Where(solution => DarwinUtils.fitnessHamming(solution.Item1) > 0)
            .Concat(selectedSolutions.Where(solution => DarwinUtils.fitnessHamming(solution.Item1, completeness()) == 0).Take(2)
            ).ToList();

        List<(ChemicalProfile, int)> crossedSolutions = new List<(ChemicalProfile, int)>();

        //Crossover
        if (Random.value <= crossoverProbability)
        {
            List<(ChemicalProfile, int)> preCrossingList = selectedSolutions.Skip(1).ToList();
            
            //Debug.Log("Crossing solutions!");
            int pivot = preCrossingList.Count / 2;
        
            preCrossingList = preCrossingList.shuffle().ToList();
        
            for (var (i, j) = (0, pivot); i < pivot && j < pivot * 2; i++, j++)
            {
                var crossedProfiles = DarwinUtils.crossoverX(preCrossingList[i].Item1, preCrossingList[j].Item1);
                crossedSolutions.Add((crossedProfiles.Item1, preCrossingList[i].Item2));
                crossedSolutions.Add((crossedProfiles.Item2, preCrossingList[j].Item2));
            }
        }
        
        //Elitarism
        if (selectedSolutions.Count > 0 && Random.value <= elitarismProbability)
        {
            (ChemicalProfile, int) tuple1 = (
                        selectedSolutions[0].Item1 + (GameManager.Instance.targetProfile - selectedSolutions[0].Item1) * 0.5f,
                        selectedSolutions[0].Item2);
                    
                    selectedSolutions.RemoveAt(0);
                    selectedSolutions.Insert(0, (new ChemicalProfile(tuple1.Item1.ChemicalElements.Select(tt => tt.amount)
                        .normalize()
                        .Zip(tuple1.Item1.ChemicalElements, (f, t) => (t._chemicalElement, f))
                        .ToDictionary(k => k._chemicalElement, v => v.f)), tuple1.Item2));
        }
        
        //Mutation
        if (Random.value <= mutationProbability)
        {
            var mutatedSolutions = new List<(ChemicalProfile, int)>();

            if (crossedSolutions.Count > 0)
            {
                for (int i = 0; i < crossedSolutions.Count; i++)
                {
                    mutatedSolutions.Add((DarwinUtils.mutation(crossedSolutions[i].Item1), crossedSolutions[i].Item2));
                }
            }
            else
            {
                for (int i = 0; i < selectedSolutions.Count; i++)
                {
                    mutatedSolutions.Add((DarwinUtils.mutation(selectedSolutions[i].Item1, ((float) i) / selectedSolutions.Count), selectedSolutions[i].Item2));
                }
            }

            selectedSolutions = mutatedSolutions;

            // selectedSolutions = crossedSolutions.Count > 0
            //     ? crossedSolutions.Select(t => (DarwinUtils.mutation(t.Item1), t.Item2)).ToList()
            //     : selectedSolutions
            //         .Select((t, index) => (DarwinUtils.mutation(t.Item1, ((float) index) / selectedSolutions.Count), t.Item2)).ToList();
        }

        //Replacing parents with children
        selectedSolutions.ForEach(t =>
        {
            population.RemoveAt(t.Item2);
            population.Insert(t.Item2, t.Item1);
        });
    }

    private float completeness()
    {
        return Mathf.Clamp(1.5f - generationStep / generationStepTarget, 0, 1.5f);
    }

    private bool maxHasChanged(ChemicalProfile cp, ChemicalProfile otherCp)
    {
        return cp.max() != otherCp.max();
    }

    public List<ChemicalProfile> Population
    {
        get => population;
        set => population = value;
    }
}