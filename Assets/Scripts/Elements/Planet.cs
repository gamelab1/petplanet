﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour, IHasChemicalProfile, IHasExtraMass
{
    public ChemicalProfile chemicalProfile;
    public ExtraMass extraMass;
    public float deteriorateTick = 5.0f;

    public float inpulseStrength = 1000.0f;
    public ForceMode2D inpulseMode = ForceMode2D.Force;

    private Dictionary<String, Action> animationEventsHandler = new Dictionary<string, Action>()
    {
        {"test", () => EventManager.RaiseToggleVolumetricEffectEvent(false, Constants.CHROMATIC_ABERRATION_NAME, 1.0f)}
    };

    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<Darwin>())
        {
            Darwin darwin = GetComponent<Darwin>();
            darwin.startEvolution(this.setChemicalProfile);
        }
        else
        {
            this.chemicalProfile = new ChemicalProfile(new Dictionary<ChemicalElement, float>());
        }

        //Hides side effects
        var (colorA, colorB, colorC) = GetComponent<GradientGenerator>().defineAndApplyColors();
        GameManager.Instance.playerPalettes[GradientTarget.Planet] = new[] {colorA, colorB, colorC};

        this.extraMass = new ExtraMass(
            0.0f,
            ChemicalProfile.Zero()
        );

        StartCoroutine(deteriorate(deteriorateTick));
    }

    private void OnEnable()
    {
        EventManager.deteriorateEvent += fuseExternalMaterial;
    }

    private void OnDisable()
    {
        EventManager.deteriorateEvent -= fuseExternalMaterial;
    }


    private void fuseExternalMaterial(float mass, ChemicalProfile profile)
    {
        var currentMass = this.GetComponent<Rigidbody2D>().mass;

        Dictionary<ChemicalElement, float> newProfileMap = new Dictionary<ChemicalElement, float>();

        for (int i = 0; i < chemicalProfile.ChemicalElements.Count; i++)
        {
            var a = chemicalProfile.ChemicalElements[i].amount * currentMass;
            var b = profile.ChemicalElements[i].amount * mass;

            newProfileMap.Add(chemicalProfile.ChemicalElements[i]._chemicalElement, (a + b) / (currentMass + mass));
        }

        this.GetComponent<Rigidbody2D>().mass += mass;

        this.chemicalProfile = new ChemicalProfile(newProfileMap);
        this.GetComponent<Darwin>().Population.RemoveAt(this.GetComponent<Darwin>().bestIndex);
        this.GetComponent<Darwin>().Population.Insert(this.GetComponent<Darwin>().bestIndex, this.chemicalProfile);
        this.GetComponent<Darwin>().bestProfile = this.chemicalProfile;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Inpulse"))
        {
            var inpulsePosition = other.transform.position;

            var direction = inpulsePosition - transform.position;

            Debug.Log(String.Format("Distance from origin {0}", (inpulsePosition - transform.position).magnitude));

            GetComponent<Rigidbody2D>().AddForceAtPosition(
                (transform.position - inpulsePosition) * inpulseStrength / (direction.magnitude * direction.magnitude),
                transform.position,
                inpulseMode
            );
        }
    }

    private IEnumerator deteriorate(float ttl)
    {
        while (true)
        {
            yield return new WaitForSeconds(ttl);

            Debug.Log("I am losing mass");

            ExtraMass deterioratedMass = new ExtraMass(this.extraMass.Mass * 0.05f, this.extraMass.ChemicalProfile);

            this.extraMass -= deterioratedMass;

            EventManager.RaiseDeteriorateEvent(deterioratedMass);
        }
    }

    public void handleEventRequestFromAnimation(String functionReference)
    {
        if (animationEventsHandler.ContainsKey(functionReference))
        {
            animationEventsHandler[functionReference].Invoke();
        }
    }

    public ChemicalProfile getChemicalProfile()
    {
        return this.chemicalProfile;
    }

    public void setChemicalProfile(ChemicalProfile cp)
    {
        this.chemicalProfile = cp;
    }

    public ExtraMass getExtraMass()
    {
        return extraMass;
    }

    public void giveExtraMass(ExtraMass extraMass)
    {
        this.extraMass += extraMass;
        EventManager.RaiseChangeColorEvent(GradientTarget.Rings, this.extraMass.ChemicalProfile);

        EventManager.RaiseMilestoneCheckEvent(
            MilestoneType.Satellite,
            () => this.extraMass.Mass >= Constants.EXTRA_MASS_THREESHOLD);
    }
}