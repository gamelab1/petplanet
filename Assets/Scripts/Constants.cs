public class Constants
{
    public const float GRAVITATIONAL_CONSTANT = 6.674E-11f;
    public const float GRAVITATIONAL_MULTIPLIER = 1E+5f;
    public const float INTENSITY_MULTIPLIER = 1E+3f;
    public const float PINCH_MOBILE_MULTIPLIER = 1E-2f;

    public const float EXTRA_MASS_THREESHOLD = 1E+4f;

    public const string CHROMATIC_ABERRATION_NAME = "ChromaticAberration(Clone)";
    public const string MOTION_BLUR_NAME = "MotionBlur(Clone)";

    public const int MILESTONE_DEFAULT_CAP = 10;

    //ShaderProperties
    
    //Planet
    
    public const string PLANET_ROTATION_SPEED_REF = "RotationSpeed_Ref";
    public const string PLANET_RING_COMPLETENESS_REF = "RingCompleteness_ref";
    
    
    //Rendered UI
    
    public const string UI_COLLISION_FACE_COLOR_REF = "_FaceColor";
}