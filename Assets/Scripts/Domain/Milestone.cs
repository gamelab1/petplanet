using System;
using UnityEngine;

[Serializable]
public struct Milestone
{
    [SerializeField] private MilestoneType milestoneType; 
    [SerializeField] private int level;
    [SerializeField] private int levelCap;

    public Milestone(MilestoneType milestoneType, int levelCap)
    {
        this.milestoneType = milestoneType;
        this.levelCap = levelCap;
        this.level = 0;
    }
    
    public Milestone(MilestoneType milestoneType, int levelCap, int level)
    {
        this.milestoneType = milestoneType;
        this.levelCap = levelCap;
        this.level = level;
    }

    public MilestoneType MilestoneType => milestoneType;

    public int Level
    {
        get => level;
    }

    public int LevelCap
    {
        get => levelCap;
    }

    public Milestone levelUp()
    {
        return new Milestone(milestoneType, levelCap, Mathf.Clamp(level + 1, 0, levelCap));
    }
}