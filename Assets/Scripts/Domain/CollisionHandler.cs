using System;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler<T> : ICollisionHandler
{
    public Func<GeneratorManager, List<(GameObject, IEnumerable<T>)>, IEnumerable<T>> collisionHandler { get; }

    public CollisionHandler(Func<GeneratorManager, List<(GameObject, IEnumerable<T>)>, IEnumerable<T>> collisionHandler)
    {
        this.collisionHandler = collisionHandler;
    }
        
}