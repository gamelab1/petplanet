﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Utilities;
using Random = UnityEngine.Random;

[Serializable]
public struct ChemicalProfile
{
    private Dictionary<ChemicalElement, float> chemicalMap;
    [SerializeField] List<ChemicalElementTuple> _chemicalElements;

    public ChemicalProfile(List<float> values)
    {
        this.chemicalMap = new Dictionary<ChemicalElement, float>();
        this._chemicalElements = new List<ChemicalElementTuple>();
    }

    public ChemicalProfile(Dictionary<ChemicalElement, float> chemicalMap, float weight)
    {
        if (chemicalMap.Count > 0)
        {
            this.chemicalMap = chemicalMap;
        }
        else
        {

            this.chemicalMap = new Dictionary<ChemicalElement, float>();
            
            var chemicalElementsArray = ChemicalElementHandler.chemicalElements.Length > 0
                ? ChemicalElementHandler.chemicalElements
                : Enum.GetValues(typeof(ChemicalElement)).Cast<ChemicalElement>().ToArray();

            chemicalElementsArray = Utils<ChemicalElement>.shuffle(chemicalElementsArray);

            float total = 1.0f;

            for (int i = 0; i < chemicalElementsArray.Length - 1; i++)
            {
                var amount = Random.Range(0, total);
                total -= amount;
                this.chemicalMap.Add(chemicalElementsArray[i], amount * weight);
            }
            
            this.chemicalMap.Add(chemicalElementsArray[chemicalElementsArray.Length - 1], total * weight);
            
        }

        List<ChemicalElementTuple> chemicalElementTuples = new List<ChemicalElementTuple>();

        foreach (var pair in this.chemicalMap)
        {
            chemicalElementTuples.Add(new ChemicalElementTuple(pair.Key, pair.Value));
        }
        
        chemicalElementTuples.Sort((a, b) => a._chemicalElement - b._chemicalElement);

        _chemicalElements = chemicalElementTuples;
    }
    
    public ChemicalProfile(Dictionary<ChemicalElement, float> chemicalMap)
    {
        if (chemicalMap.Count > 0)
        {
            this.chemicalMap = chemicalMap;
        }
        else
        {

            this.chemicalMap = new Dictionary<ChemicalElement, float>();
            
            var chemicalElementsArray = ChemicalElementHandler.chemicalElements.Length > 0
                ? ChemicalElementHandler.chemicalElements
                : Enum.GetValues(typeof(ChemicalElement)).Cast<ChemicalElement>().ToArray();

            chemicalElementsArray = Utils<ChemicalElement>.shuffle(chemicalElementsArray);

            float total = 1.0f;

            for (int i = 0; i < chemicalElementsArray.Length - 1; i++)
            {
                var amount = Random.Range(0, total);
                total -= amount;
                this.chemicalMap.Add(chemicalElementsArray[i], amount);
            }
            
            this.chemicalMap.Add(chemicalElementsArray[chemicalElementsArray.Length - 1], total);
            
            // var chemicalElementPartialDistribution = Enum.GetValues(typeof(ChemicalElement)).Cast<ChemicalElement>()
            //     .shuffle()
            //     .ToList()
            //     .Aggregate((1.0f, new List<(ChemicalElement, float)>()), (tuple, element) =>
            //     {
            //         var elementAmount = Random.Range(0, tuple.Item1);
            //         var newTuple = (element, elementAmount);
            //         tuple.Item2.Add(newTuple);
            //         return (tuple.Item1 - elementAmount, tuple.Item2);
            //     });
            //
            // this.chemicalMap = chemicalElementPartialDistribution.Item2
            //     .Select(e => (e.Item1,e.Item2
            //                           + chemicalElementPartialDistribution.Item1 / chemicalElementPartialDistribution.Item2.Count))
            //     .ToDictionary(tK => tK.Item1, tV => tV.Item2);
        }

        List<ChemicalElementTuple> chemicalElementTuples = new List<ChemicalElementTuple>();

        foreach (var pair in this.chemicalMap)
        {
            chemicalElementTuples.Add(new ChemicalElementTuple(pair.Key, pair.Value));
        }
        
        chemicalElementTuples.Sort((a, b) => a._chemicalElement - b._chemicalElement);

        _chemicalElements = chemicalElementTuples;
    }

    public static ChemicalProfile Zero()
    {
        return new ChemicalProfile(ChemicalElementHandler.chemicalElements
            .ToDictionary(k => k, v => 0.0f));
    }

    public static ChemicalProfile operator *(ChemicalProfile p, float value)
    {
        var tmp = new ChemicalProfile(p.chemicalMap
            .ToDictionary(
                k => k.Key,
                v => Math.Abs(v.Value * value)));
        return tmp;
    }

    //Does not guarantee that the cumulative sum is 1
    public static ChemicalProfile operator +(ChemicalProfile p, ChemicalProfile other)
    {
        Dictionary<ChemicalElement, float> result = new Dictionary<ChemicalElement, float>();

        foreach (var keyValue in p.chemicalMap)
        {
            result.Add(keyValue.Key, keyValue.Value + other.chemicalMap[keyValue.Key]);
        }

        return new ChemicalProfile(result);
    }
    
    //Does not guarantee that the cumulative sum is 1
    public static ChemicalProfile operator -(ChemicalProfile p, ChemicalProfile other)
    {
        return new ChemicalProfile(p.chemicalMap
            .ToDictionary(
                k => k.Key,
                v => v.Value - other.chemicalMap[v.Key])
        );
    }

    public Dictionary<ChemicalElement, float> ChemicalMap
    {
        get { return chemicalMap; }
        set { chemicalMap = value;  }
    }
    
    public List<ChemicalElementTuple> ChemicalElements
    {
        get { return _chemicalElements; }
    }
    public ChemicalElement max()
    {
        return chemicalMap.OrderBy(v => v.Value).Last().Key;
    }

    public ChemicalElement min()
    {
        return chemicalMap.OrderBy(v => v.Value).First().Key;
    }
    
}