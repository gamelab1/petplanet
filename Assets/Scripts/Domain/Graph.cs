using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Graph<T> where T : Object
{
    public HashSet<T> vertices;
    public HashSet<Edge<T>> edges;

    public Dictionary<T, HashSet<Edge<T>>> _VE_dictionary;
    public Dictionary<T, HashSet<Edge<T>>> _VE_DifferenceDictionary;

    public Dictionary<Edge<T>, HashSet<Edge<T>>> disconnectedEdges;

    public Graph(List<(T, T)> edges) : this(extractVerticesFromEdges(edges), edges)
    {
    }

    public Graph(List<T> vertices, List<(T, T)> edges)
    {
        this.vertices = new HashSet<T>();
        this.edges = new HashSet<Edge<T>>();

        for (int i = 0; i < vertices.Count; i++)
        {
            this.vertices.Add(vertices[i]);
        }

        for (int i = 0; i < edges.Count; i++)
        {
            this.edges.Add(new Edge<T>(edges[i].Item1, edges[i].Item2, null));
        }

        _VE_dictionary = new Dictionary<T, HashSet<Edge<T>>>();
        _VE_DifferenceDictionary = new Dictionary<T, HashSet<Edge<T>>>();
        disconnectedEdges = new Dictionary<Edge<T>, HashSet<Edge<T>>>();
        
        IEnumerator<Edge<T>> enumeratorEdges = this.edges.GetEnumerator();
        
        for (; enumeratorEdges.MoveNext();)
        {
            Edge<T> e = enumeratorEdges.Current;

            if (!_VE_dictionary.ContainsKey(e.vertexA))
            {
                _VE_dictionary.Add(e.vertexA, new HashSet<Edge<T>>());
            }
          
            if (!_VE_dictionary.ContainsKey(e.vertexB))
            {
                _VE_dictionary.Add(e.vertexB, new HashSet<Edge<T>>());
            }

            _VE_dictionary[e.vertexB].Add(e);
            _VE_dictionary[e.vertexA].Add(e);

            IEnumerator<T> enumeratorVertices = this.vertices.GetEnumerator();
            
            for (; enumeratorVertices.MoveNext();)
            {
                if (enumeratorVertices.Current == e.vertexA || enumeratorVertices.Current == e.vertexB)
                {
                    continue;
                }
                
                T vertex = enumeratorVertices.Current;
                
                if (!_VE_DifferenceDictionary.ContainsKey(vertex))
                {
                    _VE_DifferenceDictionary.Add(vertex, new HashSet<Edge<T>>());
                }

                _VE_DifferenceDictionary[vertex].Add(e);
            }
        }

        enumeratorEdges = this.edges.GetEnumerator();

        for (; enumeratorEdges.MoveNext();)
        {
            var e = enumeratorEdges.Current;

            var vertexA = e.vertexA;
            var vertexB = e.vertexB;

            if (!disconnectedEdges.ContainsKey(e))
            {
                disconnectedEdges.Add(e, new HashSet<Edge<T>>());
            }

            var disconnectedEdgesToA = this._VE_DifferenceDictionary[vertexA];
            var disconnectedEdgesToB = this._VE_DifferenceDictionary[vertexB];
            
            disconnectedEdges[e].UnionWith(disconnectedEdgesToA);
            disconnectedEdges[e].IntersectWith(disconnectedEdgesToB);
            //
            // foreach (var currentVertex in _VE_dictionary.Keys)
            // {
            //     if (currentVertex.GetInstanceID().Equals(vertexA.GetInstanceID()) ||
            //         currentVertex.GetInstanceID().Equals(vertexB.GetInstanceID()))
            //     {
            //         continue;
            //     }
            //
            //     disconnectedEdges[e].UnionWith(_VE_dictionary[currentVertex]);
            // }
            
        }

    }

    private static List<T> extractVerticesFromEdges(List<(T, T)> edges)
    {
        HashSet<T> vertices = new HashSet<T>();

        for (int i = 0; i < edges.Count; i++)
        {
            vertices.Add(edges[i].Item1);
            vertices.Add(edges[i].Item2);
        }

        return vertices.ToList();
    }

    public class Edge<T> where T : Object
    {
        public T vertexA;
        public T vertexB;

        public T instance;

        public Edge(T vertexA, T vertexB, T instance)
        {
            this.vertexA = vertexA;
            this.vertexB = vertexB;
            this.instance = instance;
        }

        public T Instance
        {
            get => instance;
            set => instance = value;
        }

        public class EdgeEqualityComparer : IEqualityComparer<Edge<T>>
        {
            public bool Equals(Edge<T> x, Edge<T> y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.vertexA.GetInstanceID() + x.vertexB.GetInstanceID() ==
                       y.vertexA.GetInstanceID() + y.vertexB.GetInstanceID();
            }

            public int GetHashCode(Edge<T> obj)
            {
               return (obj.vertexA.GetInstanceID() + obj.vertexB.GetInstanceID()).GetHashCode();
            }
        }
    }
}