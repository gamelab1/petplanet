using System.Collections.Generic;
using UnityEngine;

public struct AsteroidCollision
{
    public GameObject first { get; }
    public GameObject second { get; }

    public AsteroidCollision(GameObject a, GameObject b)
    {
        this.first = a;
        this.second = b;
    }
    
    public class AsteroidCollisionEqualityComparer : IEqualityComparer<AsteroidCollision>
    {
        public bool Equals(AsteroidCollision x, AsteroidCollision y)
        {
            if (x.first.GetInstanceID() + x.second.GetInstanceID() == y.first.GetInstanceID() + y.second.GetInstanceID())
            {
                return true;
            }
            return false;
        }

        public int GetHashCode(AsteroidCollision obj)
        {
            return (obj.first.GetInstanceID() + obj.second.GetInstanceID()).GetHashCode();
        }
    }
}