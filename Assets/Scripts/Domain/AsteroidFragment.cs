using UnityEngine;

public class Fragment
{
    public float mass { get; set; }
    public float size { get; set; }
    public ChemicalProfile chemicalProfile { get; set; }

    public Vector2 position { get; set; }

    public Vector2 velocity { get; set; }
}
public class AsteroidFragment : Fragment
{
    public AsteroidFragment(float mass, float size, ChemicalProfile cp, Vector2 position, Vector2 velocity)
    {
        this.mass = mass;
        this.size = size;
        this.chemicalProfile = cp;
        this.velocity = velocity;
        this.position = position;
    }
}