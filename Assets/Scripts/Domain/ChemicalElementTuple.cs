using System;
using UnityEngine;

[Serializable]
public struct ChemicalElementTuple
{
    [SerializeField] public ChemicalElement _chemicalElement;
    [SerializeField] public float amount;
    public Color Color;

    public ChemicalElementTuple(ChemicalElement chemicalElement, float amount)
    {
        this._chemicalElement = chemicalElement;
        this.amount = amount;
        this.Color = this._chemicalElement.getColor();
    }
}