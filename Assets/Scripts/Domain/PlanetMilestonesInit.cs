using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMilestonesInit : MonoBehaviour
{
    private Dictionary<MilestoneType, MilestoneMetadata> _milestoneLevelUps =
        new Dictionary<MilestoneType, MilestoneMetadata>()
        {
            {MilestoneType.Satellite, new MilestoneMetadata(new Dictionary<int, ActionOnMilestoneLevelUp>() {
                    {0, new ActionOnMilestoneLevelUp(0.2f, Constants.PLANET_RING_COMPLETENESS_REF)},
                    {1, new ActionOnMilestoneLevelUp(0.4f, Constants.PLANET_RING_COMPLETENESS_REF)},
                    {2, new ActionOnMilestoneLevelUp(0.6f, Constants.PLANET_RING_COMPLETENESS_REF)},
                    {3, new ActionOnMilestoneLevelUp(0.8f, Constants.PLANET_RING_COMPLETENESS_REF)},
                    {4, new ActionOnMilestoneLevelUp(1.3f, Constants.PLANET_RING_COMPLETENESS_REF)}
                }, 5.0f, 0)
            }
        };

    private Material _material;

    private void Start()
    {
        Planet planet = GetComponent<Planet>();
        _material = GetComponent<Renderer>().material;
        
        //Set up milestones conditions

        MilestoneType.Satellite.registerLevelCheckMap(new List<Func<bool>>()
        {
            {
                () => { 
                    return planet.extraMass.Mass > Constants.EXTRA_MASS_THREESHOLD + 1E+4f * 1;
                }
            },
            {
                () => { 
                    return planet.extraMass.Mass > Constants.EXTRA_MASS_THREESHOLD + 1E+4f * 2;
                }
            },
            {
                () => { 
                    return planet.extraMass.Mass > Constants.EXTRA_MASS_THREESHOLD + 1E+4f * 3;
                }
            },
            {
                () => { 
                    return planet.extraMass.Mass > Constants.EXTRA_MASS_THREESHOLD + 1E+4f * 4;
                }
            },
            {
                () => { 
                    return false;
                }
            }
        }.ToArray());
        
        MilestoneType.Constellations.registerLevelCheckMap(new List<Func<bool>>() {
            {
                () => true
            },
            {
                () => true
            },
            {
                () => true
            },
            {
                () => true
            }
        }.ToArray());
    }

    private void OnEnable()
    {
        EventManager.milestoneLevelUpEvent += onSatelliteLevelUp;
        EventManager.milestoneLevelUpEvent += onConstellationLevelUp;
    }

    private void OnDisable()
    {
        EventManager.milestoneLevelUpEvent -= onSatelliteLevelUp;
        EventManager.milestoneLevelUpEvent -= onConstellationLevelUp;
    }

    public void onConstellationLevelUp(MilestoneType milestoneType, int level)
    {
        if (milestoneType == MilestoneType.Constellations)
        {
            Debug.Log(MilestoneType.Constellations + " reached Level " + level);
        }
    }

    public void onSatelliteLevelUp(MilestoneType milestoneType, int level)
    {
        if (milestoneType == MilestoneType.Satellite)
        {
            Debug.Log(MilestoneType.Satellite + " reached Level " + level);

            MilestoneMetadata milestoneMetadata = _milestoneLevelUps[MilestoneType.Satellite];

            StartCoroutine(lerpOverFrames(milestoneMetadata, level));
        }
    }

    IEnumerator lerpOverFrames(MilestoneMetadata milestoneMetadata, int level)
    {
        while (milestoneMetadata.ElapsedTime < milestoneMetadata.Time)
        {
            yield return new WaitForFixedUpdate(); //Run this every FixedUpdate
            
            String propertyRef = milestoneMetadata.MilestoneLevelUps[level].ShaderProperty;

            milestoneMetadata.ElapsedTime += Time.fixedDeltaTime;

            float step = Mathf.Clamp(milestoneMetadata.ElapsedTime / milestoneMetadata.Time, 0, 1);
            
            var tmp = Utilities.Utils<float>.blendQuantity(milestoneMetadata.CurrentLevel, milestoneMetadata.MilestoneLevelUps[level].TargetValue, step);

            _material.SetFloat(Shader.PropertyToID(propertyRef), tmp);
        }

        milestoneMetadata.ElapsedTime = 0;
        _material.SetFloat(
            Shader.PropertyToID(milestoneMetadata.MilestoneLevelUps[level].ShaderProperty), 
            milestoneMetadata.MilestoneLevelUps[level].TargetValue
            );
        milestoneMetadata.CurrentLevel = milestoneMetadata.MilestoneLevelUps[level].TargetValue;
    }

    private class MilestoneMetadata
    {
        private Dictionary<int, ActionOnMilestoneLevelUp> milestoneLevelUps;
        private float time;
        private float elapsedTime = 0.0f;
        private float currentLevel = 0f;

        public MilestoneMetadata(Dictionary<int, ActionOnMilestoneLevelUp> milestoneLevelUps, float time, float currentLevel)
        {
            this.milestoneLevelUps = milestoneLevelUps;
            this.time = time;
            this.currentLevel = currentLevel;
        }

        public Dictionary<int, ActionOnMilestoneLevelUp> MilestoneLevelUps
        {
            get => milestoneLevelUps;
            set => milestoneLevelUps = value;
        }

        public float Time
        {
            get => time;
            set => time = value;
        }

        public float ElapsedTime
        {
            get => elapsedTime;
            set => elapsedTime = value;
        }

        public float CurrentLevel
        {
            get => currentLevel;
            set => currentLevel = value;
        }
    }
    
    private struct ActionOnMilestoneLevelUp
    {
        private float _targetValue;
        private string _shaderProperty;

        public ActionOnMilestoneLevelUp(float targetValue, string shaderProperty)
        {
            this._targetValue = targetValue;
            this._shaderProperty = shaderProperty;
        }

        public float TargetValue
        {
            get => _targetValue;
            set => _targetValue = value;
        }

        public string ShaderProperty
        {
            get => _shaderProperty;
            set => _shaderProperty = value;
        }
    }
}