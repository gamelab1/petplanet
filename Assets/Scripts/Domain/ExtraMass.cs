using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

[Serializable]
public struct ExtraMass
{
    [SerializeField] private float mass;
    [SerializeField] private ChemicalProfile chemicalProfile;

    public ExtraMass(float mass, ChemicalProfile chemicalProfile)
    {
        this.mass = mass;
        this.chemicalProfile = chemicalProfile;
    }
    
    public float Mass
    {
        get { return this.mass; }
    }

    public ChemicalProfile ChemicalProfile
    {
        get { return this.chemicalProfile; }
    }

    public static ExtraMass operator +(ExtraMass p, ExtraMass massChemicalProfileTuple)
    {
        float weightedMass = massChemicalProfileTuple.mass * Random.Range(0.6f, 0.9f);
        return p.merge(new ExtraMass(weightedMass, massChemicalProfileTuple.chemicalProfile), 1);
    }

    public static ExtraMass operator -(ExtraMass extraMass, ExtraMass massToSubtract)
    {
        return extraMass.merge(
            massToSubtract,
            -1
        );
    }

    private ExtraMass merge(ExtraMass other, float multiplier)
    {
        float totalMass = Mathf.Max(this.mass + other.mass * multiplier, 0.0f);
        List<ChemicalElement> chemicalElements = this.chemicalProfile.ChemicalMap.Select(kV => kV.Key).ToList();

        List<ChemicalElementTuple> chemicalElementTuples = this.chemicalProfile.ChemicalElements;
        
        ExtraMass tmpThis = this;

        //TODO to change with normal for loops
        return new ExtraMass(
            totalMass,
            new ChemicalProfile(chemicalElementTuples
                .Select(tupla => totalMass > 0
                    ? Mathf.Max(tmpThis.mass * tupla.amount
                                + other.mass * other.chemicalProfile.ChemicalMap[tupla._chemicalElement] * multiplier, 0.0f) / totalMass
                    : 0.0f)
                .normalize()
                .Zip(chemicalElementTuples, (f, t) => (t._chemicalElement, f))
                .ToDictionary(k => k._chemicalElement, v => v.f))
        );
    }
}