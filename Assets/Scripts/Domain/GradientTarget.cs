using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum GradientTarget
{
    Planet,
    Rings
}

public static class GradientTargetHandler
{
    private static Dictionary<GradientTarget, (String[], Color[])> gradientTargetDescription =
        new Dictionary<GradientTarget, (string[], Color[])>()
        {
            {
                GradientTarget.Planet, (
                    new[]
                    {
                        "GradientColor_A_ref",
                        "GradientColor_B_ref",
                        "GradientColor_C_ref",
                    }, new []{Color.black, Color.black, Color.black})
            },
            {
                GradientTarget.Rings, (
                    new[]
                    {
                        "RingGradientInnerColor_ref", 
                        "RingGradientRadialColor_ref"
                    }, new [] { Color.black, Color.black})
            }
        };

    public static string[] shaderReferenceStrings(this GradientTarget target)
    {
        return gradientTargetDescription[target].Item1;
    }
    
    public static Color[] baseColors(this GradientTarget target)
    {
        return gradientTargetDescription[target].Item2;
    }
}