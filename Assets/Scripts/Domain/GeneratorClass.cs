using System;

[Serializable]
public enum GeneratorClass
{
    Asteroid,
    Trash
}