using System;
using System.Collections.Generic;

[Serializable]
public enum MilestoneType
{
    ChemicalComposition,
    Constellations,
    Orbit,
    Satellite
}

public static class MilestoneTypeHandler
{
    private static Dictionary<MilestoneType, Func<bool>[]> milestoneTypeLevelCheckMap =
        new Dictionary<MilestoneType, Func<bool>[]>();

    public static Dictionary<MilestoneType, int> milestoneLevelCap = new Dictionary<MilestoneType, int>()
    {
        {MilestoneType.ChemicalComposition, Constants.MILESTONE_DEFAULT_CAP},
        {MilestoneType.Constellations, Constants.MILESTONE_DEFAULT_CAP},
        {MilestoneType.Orbit, Constants.MILESTONE_DEFAULT_CAP},
        {MilestoneType.Satellite, Constants.MILESTONE_DEFAULT_CAP}
    };
    

    public static List<MilestoneType> milestoneTypes = new List<MilestoneType>()
    {
        MilestoneType.ChemicalComposition,
        MilestoneType.Constellations,
        MilestoneType.Orbit,
        MilestoneType.Satellite
    };

    public static void registerLevelCheckMap(this MilestoneType milestoneType, Func<bool>[] levelCheckMap)
    {
        if (milestoneTypeLevelCheckMap.ContainsKey(milestoneType))
        {
            milestoneTypeLevelCheckMap[milestoneType] = levelCheckMap;
        }
        else
        {
            milestoneTypeLevelCheckMap.Add(milestoneType, levelCheckMap);
        }
    }
    
    public static Func<bool>[] getLevelCheckMap(this MilestoneType milestoneType)
    {
        return milestoneTypeLevelCheckMap.ContainsKey(milestoneType)
            ? milestoneTypeLevelCheckMap[milestoneType]
            : new Func<bool>[0];
    }
}