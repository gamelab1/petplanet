using UnityEngine;

public class TestScript : MonoBehaviour
{

    public GameObject button;
    
    private void OnTriggerEnter(Collider other)
    {
        if (button != null && other.CompareTag("Player"))
        {
            button.SetActive(true);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (button != null && other.CompareTag("Player"))
        {
            button.SetActive(false);
        }
    }
}
