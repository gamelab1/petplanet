using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GraphMonitor : MonoBehaviour
{
    public TextAsset textAsset;
    public GameObject nodeTemplate;
    public GameObject edgeTemplate;
    public GameObject graphContainerTemplate;

    private List<Graph<GameObject>> constellations;

    public bool isPlanar = false;

    public GameObject selectedNode;

    private int maxConstellationMilestoneLevel;

    public bool isConstellationGamePlaying = false;
    public bool canDrag = true;

    private Coroutine constellationMiniGameRoutine = null;

    public CameraMovement cameraMovement;

    private void OnEnable()
    {
        EventManager.touchBeganEvent += selectNode;
        EventManager.touchMovedEvent += moveNode;
        EventManager.touchEndedEvent += deselectNode;
        EventManager.constellationGameStartEvent += constellationGameStart;
        EventManager.constellationGameEndEvent += constellationGameEnd;
    }

    private void OnDisable()
    {
        EventManager.touchBeganEvent -= selectNode;
        EventManager.touchMovedEvent -= moveNode;
        EventManager.touchEndedEvent -= deselectNode;
        EventManager.constellationGameStartEvent -= constellationGameStart;
        EventManager.constellationGameEndEvent -= constellationGameEnd;
    }
    
    private void constellationGameStart(int constellationIndex)
    {
        Debug.Log("Constellation game start!");
        isConstellationGamePlaying = true;
        canDrag = true;
        isPlanar = false;

        Graph<GameObject> constellationSelected = this.constellations[constellationIndex];

        var vertices = constellationSelected.vertices.GetEnumerator();

        GameObject selectedVert = null;

        for (; vertices.MoveNext();)
        {
            selectedVert = vertices.Current;

            drawGraph(
                new Vector3(
                    8.5f * (UnityEngine.Random.value - 0.5f),
                    8.5f * (UnityEngine.Random.value - 0.5f),
                    selectedVert.transform.transform.position.z),
                selectedVert,
                constellationSelected,
                (g, p) => g.transform.localPosition = p
            );
        }

        selectedVert.transform.parent.gameObject.SetActive(true);
        
        cameraMovement.StartCoroutine(cameraMovement.moveToPosition(
            selectedVert.transform.parent,
            selectedVert.transform.parent.GetComponent<CircleCollider2D>(), 
            1f)
        );
    }

    private void constellationGameEnd(int constellationIndex)
    {
        Debug.Log("Constellation game ended!");
        isConstellationGamePlaying = false;
        canDrag = false;

        //Get milestone points
        EventManager.RaiseMilestoneCheckEvent(
            MilestoneType.Constellations,
            () => this.isPlanar
            );

        //Start a new constellation game routine
        constellationMiniGameRoutine = StartCoroutine(constellationMiniGameTrigger());
        
        cameraMovement.StartCoroutine(cameraMovement.moveToPosition(
            GameManager.Instance.cameraTarget.Current.Item1,
            GameManager.Instance.cameraTarget.Current.Item1.GetComponent<CircleCollider2D>(), 
            1f)
        );
    }

    private void selectNode(MouseMovement movement)
    {
        int layerMask = LayerMask.NameToLayer("ConstelationNodes");

        layerMask = ~layerMask;

        Ray ray = Camera.main.ScreenPointToRay(movement.CurrentPosition);

        Debug.DrawRay(ray.origin, ray.direction, Color.magenta, 1.0f);

        RaycastHit hitInfo = new RaycastHit();
        if (Physics.Raycast(ray, out hitInfo, 100, layerMask))
        {
            if (hitInfo.transform.gameObject.CompareTag("StarNode"))
            {
                selectedNode = hitInfo.transform.gameObject;
            }
        }
    }

    private void moveNode(MouseMovement movement)
    {
        if (selectedNode && canDrag)
        {
            var dest = Camera.main.ScreenToWorldPoint(movement.CurrentPosition);
            drawGraph(
                new Vector3(dest.x, dest.y, selectedNode.transform.position.z), 
                selectedNode, 
                constellations[0],
                (g, p) => g.transform.position = p
                );

            this.isPlanar = isAValidConstellation(constellations[0]);

            if (this.isPlanar && this.isConstellationGamePlaying)
            {
                Debug.Log("Constellation is planar!");
                StartCoroutine(popUpGameOver(0));
            }
        }
    }

    IEnumerator popUpGameOver(int index)
    {
        this.isConstellationGamePlaying = false;
        while (this.isPlanar)
        {
            yield return new WaitForSeconds(2.0f); //Let the user put the node where she wants

            if (this.isPlanar)
            {
                this.isConstellationGamePlaying = false;
                this.canDrag = false;
                
                //Spawn a toast
                EventManager.RaiseUICreateCommandEvent(
                    "Great! You solved a constellation puzzle! Thanks!",
                    2f,
                    a => Debug.Log(a),
                    "Callback Fired"
                );
                
                EventManager.RaiseConstellationGameEndEvent(index);
                break;
            }
            else
            {
                this.isConstellationGamePlaying = true;
                this.canDrag = true;
            }
        }
    }

    private void deselectNode(MouseMovement movement)
    {
        selectedNode = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        this.maxConstellationMilestoneLevel = GameManager.Instance.milestones[MilestoneType.Constellations].LevelCap;
        this.cameraMovement = Camera.main.GetComponent<CameraMovement>();

        var lines = textAsset.text.Split('\n');

        var edges = new HashSet<Graph<GameObject>.Edge<GameObject>>();
        var nodes = new HashSet<string>();
        var nodesGO = new Dictionary<string, GameObject>();

        var parents = new List<GameObject>();


        var currentParent = GameObject.Instantiate(graphContainerTemplate, transform);

        currentParent.name = "Graph_0";
        currentParent.transform.localPosition = Vector3.zero;

        parents.Add(currentParent);

        constellations = new List<Graph<GameObject>>();

        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].Equals("@"))
            {
                currentParent = GameObject.Instantiate(graphContainerTemplate, transform);
                currentParent.name = $"Graph_{parents.Count}";
                currentParent.transform.localPosition = Vector3.zero;
                parents.Add(currentParent);

                constellations.Add(new Graph<GameObject>(edges.Select(e => (e.vertexA, e.vertexB)).ToList()));

                edges = new HashSet<Graph<GameObject>.Edge<GameObject>>();
                nodes = new HashSet<string>();
                nodesGO = new Dictionary<string, GameObject>();

                continue;
            }

            var tuple = lines[i].Split(',');

            if (nodes.Add(tuple[0]))
            {
                GameObject go = GameObject.Instantiate(nodeTemplate, currentParent.transform);
                go.name = tuple[0];
                nodesGO.Add(tuple[0], go);
            }

            if (nodes.Add(tuple[1]))
            {
                GameObject go = GameObject.Instantiate(nodeTemplate, currentParent.transform);
                go.name = tuple[1];
                nodesGO.Add(tuple[1], go);
            }

            edges.Add(new Graph<GameObject>.Edge<GameObject>(nodesGO[tuple[0]], nodesGO[tuple[1]], null));
        }

        constellations.Add(new Graph<GameObject>(edges.Select(e => (e.vertexA, e.vertexB)).ToList()));

        //Draw graph
        for (int i = 0; i < constellations.Count; i++)
        {
            Graph<GameObject> graph = constellations[i];

            IEnumerator<GameObject> verticesEnumerator = graph.vertices.GetEnumerator();

            for (; verticesEnumerator.MoveNext();)
            {
                GameObject v = verticesEnumerator.Current;

                v.transform.localPosition = new Vector3(10 * (UnityEngine.Random.value - 0.5f),
                    10 * (UnityEngine.Random.value - 0.5f),
                    v.transform.position.z);
            }

            IEnumerator<Graph<GameObject>.Edge<GameObject>> edgesEnumerator = graph.edges.GetEnumerator();

            for (; edgesEnumerator.MoveNext();)
            {
                Graph<GameObject>.Edge<GameObject> e = edgesEnumerator.Current;

                GameObject newEdge = GameObject.Instantiate(edgeTemplate, parents[i].transform);
                newEdge.transform.position = e.vertexA.transform.position;
                newEdge.name = $"{e.vertexA.name} <-> {e.vertexB.name}";

                e.Instance = newEdge;

                Vector3 edge = e.vertexB.transform.position - e.vertexA.transform.position;

                newEdge.transform.localScale = new Vector3(edge.magnitude, newEdge.transform.localScale.y,
                    newEdge.transform.localScale.z);

                newEdge.transform.rotation = Quaternion.FromToRotation(Vector3.right, edge);
            }
        }

        Debug.Log("Graph generated");

        constellationMiniGameRoutine = StartCoroutine(constellationMiniGameTrigger());
    }

    IEnumerator constellationMiniGameTrigger()
    {
        while (true)
        {
            yield return new WaitForSeconds(GameManager.Instance.constellationMiniGameTicker);

            //Triggera il minigioco con probabilità 1 - p dove p è il progresso della milestone Constellations
            float actualLevel = GameManager.Instance.milestones[MilestoneType.Constellations].Level;

            float probability = 1 - (actualLevel / maxConstellationMilestoneLevel);

            Debug.Log($"Porbability to start a constellation game {probability}");

            if (!isConstellationGamePlaying && UnityEngine.Random.value <= probability)
            {
                //Triggera l'evento di start del minigioco.
                EventManager.RaiseConstellationGameStartEvent(UnityEngine.Random.Range(0, constellations.Count));
                break;
            }
        }
    }

    private void drawGraph(Vector3 targetPos, GameObject targetObject, Graph<GameObject> constellation, Action<GameObject, Vector3> positionAssigner)
    {
        positionAssigner.Invoke(targetObject, targetPos);

        var graph = constellation;

        IEnumerator<Graph<GameObject>.Edge<GameObject>> edgesEnumerator = graph.edges.GetEnumerator();

        for (; edgesEnumerator.MoveNext();)
        {
            Graph<GameObject>.Edge<GameObject> e = edgesEnumerator.Current;

            Vector3 edge = e.vertexB.transform.position - e.vertexA.transform.position;

            e.Instance.transform.position = e.vertexA.transform.position;

            e.Instance.transform.localScale = new Vector3(edge.magnitude, e.Instance.transform.localScale.y,
                e.Instance.transform.localScale.z);

            e.Instance.transform.rotation = Quaternion.FromToRotation(Vector3.right, edge);
        }

    }

    private bool isAValidConstellation(Graph<GameObject> constellation)
    {
        List<bool> planarityChecks = new List<bool>();

        var edgesEnum = constellation.edges.GetEnumerator();

        for (; edgesEnum.MoveNext();)
        {
            Graph<GameObject>.Edge<GameObject> edge = edgesEnum.Current;

            var disconnectedEdges = constellation.disconnectedEdges[edge].GetEnumerator();

            for (; disconnectedEdges.MoveNext();)
            {
                Boolean b = intersect(edge, disconnectedEdges.Current);

                if (b)
                {
                    planarityChecks.Add(b);
                }
            }
        }

        return !planarityChecks.Any();
    }

    private bool intersect(Graph<GameObject>.Edge<GameObject> edge, Graph<GameObject>.Edge<GameObject> otherEdge)
    {
        GeometryUtils.Line line =
            GeometryUtils.getLineFor(edge.vertexA.transform.position, edge.vertexB.transform.position);
        GeometryUtils.Line otherLine =
            GeometryUtils.getLineFor(otherEdge.vertexA.transform.position, otherEdge.vertexB.transform.position);

        if (line.Coefficient != otherLine.Coefficient)
        {
            Vector3 collisionPoint = GeometryUtils.collide(line, otherLine);

            return GeometryUtils.isInsideSegment(collisionPoint, edge.vertexA.transform.position,
                       edge.vertexB.transform.position)
                   && GeometryUtils.isInsideSegment(collisionPoint, otherEdge.vertexA.transform.position,
                       otherEdge.vertexB.transform.position);
        }

        return false;
    }
}