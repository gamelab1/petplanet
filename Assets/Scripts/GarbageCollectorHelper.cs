using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageCollectorHelper : MonoBehaviour
{
    private Queue<(GameObject, Action<GameObject> callback, float)> queue =
        new Queue<(GameObject, Action<GameObject> callback, float)>();

    private void OnEnable()
    {
        EventManager.enqueueToDestroyEvent += throwGarbage;
    }

    private void OnDisable()
    {
        EventManager.enqueueToDestroyEvent -= throwGarbage;
    }

    public void throwGarbage(GameObject target, Action<GameObject> callback, float delay)
    {
        queue.Enqueue((target, callback, delay));

        StartCoroutine(collectGarbage());
    }

    IEnumerator collectGarbage()
    {
        if (queue.Count > 0)
        {
            var tuple = queue.Dequeue();

            yield return new WaitForSeconds(tuple.Item3);
            
            tuple.callback.Invoke(tuple.Item1);
            
            Destroy(tuple.Item1);
        }
    }
}