using System.Collections.Generic;
using UnityEngine;


public class GameObjectProfile
{
    private int id;
    public int ID => id;

    private Vector3 position;
    private Vector2 velocity;

    private float mass;
    private Vector3 scale;
    private Material material;
    private bool isActive;
    private Bounds bounds;

    private ExtraMass extraMass;

    private ChemicalProfile chemicalProfile;

    private List<(MilestoneType, int)> milestones; 

    private int hits;

    public GameObjectProfile()
    {
    }

    public GameObjectProfile(int id, Vector3 position, Vector2 velocity, float mass, Vector3 scale, Material material, bool isActive, Bounds bounds)
    {
        this.id = id;
        this.position = position;
        this.velocity = velocity;
        this.mass = mass;
        this.scale = scale;
        this.material = material;
        this.isActive = isActive;
        this.bounds = bounds;
        this.extraMass = new ExtraMass(
            0.0f,
            ChemicalProfile.Zero()
        );
        this.chemicalProfile = ChemicalProfile.Zero();
        this.hits = 0;
        this.milestones = new List<(MilestoneType, int)>();
    }

    public GameObjectProfile(int id, Vector3 position, Vector2 velocity, float mass, Vector3 scale, Material material,
        bool isActive, Bounds bounds, ExtraMass extraMass, ChemicalProfile chemicalProfile, 
        int globalHits, List<(MilestoneType, int)> milestones)
    {
        this.id = id;
        this.position = position;
        this.velocity = velocity;
        this.mass = mass;
        this.scale = scale;
        this.material = material;
        this.isActive = isActive;
        this.bounds = bounds;
        this.extraMass = new ExtraMass(
            extraMass.Mass,
            new ChemicalProfile(extraMass.ChemicalProfile.ChemicalMap)
                );
        this.chemicalProfile = new ChemicalProfile(chemicalProfile.ChemicalMap);
        this.hits = globalHits;
    }
    
    public int Hits
    {
        get => hits;
        set => hits = value;
    }

    public ChemicalProfile ChemicalProfile
    {
        get => chemicalProfile;
        set => chemicalProfile = value;
    }

    public ExtraMass ExtraMass
    {
        get => extraMass;
        set => extraMass = value;
    }

    public Vector3 Position
    {
        get => position;
        set => position = value;
    }

    public Vector2 Velocity
    {
        get => velocity;
        set => velocity = value;
    }

    public float Mass
    {
        get => mass;
        set => mass = value;
    }

    public Vector3 Scale
    {
        get => scale;
        set => scale = value;
    }

    public Material Material
    {
        get => material;
        set => material = value;
    }

    public bool IsActive
    {
        get => isActive;
        set => isActive = value;
    }

    public Bounds Bounds
    {
        get => bounds;
        set => bounds = value;
    }

    public List<(MilestoneType, int)> Milestones
    {
        get => milestones;
        set => milestones = value;
    }
}