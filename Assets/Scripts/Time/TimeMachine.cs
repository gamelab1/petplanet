using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;

public class TimeMachine : MonoBehaviour
{
    public float maxRotationSpeed = 5f;
    public float delayTimeMachine = 5f;

    public GameObject planet;

    private Material planetMaterial;

    private float baseTimeSpeed;

    private Deque<TimeMachineEntry> history = new Deque<TimeMachineEntry>();

    public float tick = 0.4f;

    public Coroutine timeMachineJob = null;

    public int historySize = 0;

    public bool traveling = false;

    void Start()
    {
        //Synchronize planet shader parameters to timeScale
        planetMaterial = planet.GetComponent<Renderer>().material;
        planetMaterial.SetFloat(Shader.PropertyToID(Constants.PLANET_ROTATION_SPEED_REF),
            planetMaterial.GetFloat(Shader.PropertyToID(Constants.PLANET_ROTATION_SPEED_REF)) /
            GameManager.Instance.timeScale);
        baseTimeSpeed = planetMaterial.GetFloat(Shader.PropertyToID(Constants.PLANET_ROTATION_SPEED_REF));

        timeMachineJob = StartCoroutine(timeMachineTick(tick));
    }

    private void Update()
    {
        historySize = history.Count;
    }

    IEnumerator timeMachineTick(float tick)
    {
        while (true)
        {
            yield return new WaitForSeconds(tick);
            
            //Increment favor
            GameManager.Instance.updateFavor(50.0f);
            
            if (!traveling)
            {
                history.Enqueue(TimeMachineEntry.current());
                if (history.Count > 100 / tick)
                {
                    history.Dequeue();
                }
            }
        }
    }

    public float getBaseRotationSpeed()
    {
        return baseTimeSpeed;
    }

    public float getMaxDelayTimeRestore()
    {
        return delayTimeMachine;
    }

    public float getTargetRotationSpeed(float weight, float direction)
    {
        return maxRotationSpeed * weight * -Mathf.Sign(direction);
    }

    public IEnumerator accelerateFor(float timeInSeconds, float targetRotationSpeed, bool isReverse = false)
    {
        if (timeInSeconds == 0)
        {
            traveling = true;
            
            //Trigger VFX event to get Motion Blur on
            EventManager.RaiseToggleVolumetricEffectEvent(true, Constants.MOTION_BLUR_NAME, 1.0f);
        }

        yield return new WaitForSeconds(timeInSeconds);

        traveling = timeInSeconds <= 0;

        if (timeInSeconds > 0)
        {
            //Trigger VFX event to get Motion Blur off
            EventManager.RaiseToggleVolumetricEffectEvent(false, Constants.MOTION_BLUR_NAME, 1.0f);
            
            if (isReverse) //Going back in time
            {
                for (var i = 0; i <= Mathf.Min(50 * (delayTimeMachine / timeInSeconds) / tick, historySize - 2); i++)
                {
                    history.Pop();
                }

                TimeMachineEntry lastSnapshot = history.Peek();


                Utils<Transform>.ToIEnumerable(GameManager.Instance.mainPool.GetEnumerator())
                    .SelectMany(t => Utils<Transform>.ToIEnumerable(t.GetEnumerator()),
                        (t, child) => (t.tag, child))
                    .ToList()
                    .ForEach(tuple =>
                    {
                        var gp = lastSnapshot.Profiles[tuple.tag].ContainsKey(tuple.child.gameObject.GetInstanceID())
                            ? lastSnapshot.Profiles[tuple.tag][tuple.child.gameObject.GetInstanceID()]
                            : null;

                        if (gp != null)
                        {
                            copyToGameObject(tuple.child.gameObject, gp);
                        }
                        else
                        {
                            tuple.child.gameObject.SetActive(false);
                        }
                    });
            }
            else
            {
                TimeMachineEntry lastSnapshot = history.Peek();

                GameObjectProfile playerProfile = lastSnapshot.Profiles["PlanetsPool"].Values
                    .FirstOrDefault(gp => gp.ID == GameManager.Instance.Player.GetInstanceID());

                Utils<Transform>.ToIEnumerable(GameManager.Instance.mainPool.GetEnumerator())
                    .SelectMany(t => Utils<Transform>.ToIEnumerable(t.GetEnumerator()),
                        (t, child) => (t.tag, child))
                    .Where(tuple => tuple.child.gameObject.activeSelf)
                    .ToList()
                    .ForEach(tuple =>
                    {
                        var gp = lastSnapshot.Profiles[tuple.tag].ContainsKey(tuple.child.gameObject.GetInstanceID())
                            ? lastSnapshot.Profiles[tuple.tag][tuple.child.gameObject.GetInstanceID()]
                            : null;

                        if (gp != null)
                        {
                            //Calculate next position after timeInSeconds * timeMultiplier
                            tuple.child.position = getPositionInTheFuture(gp, timeInSeconds);
                            //Check if the object collides with player.
                            // if (gp.ID != playerProfile.ID)
                            // {
                            //     var collides = Utils.collidesWith(gp, playerProfile,
                            //         timeInSeconds / GameManager.Instance.timeScale);
                            //
                            //     if (collides)
                            //     {
                            //         Debug.Log($"{gp.ID} collide con il player");
                            //     }
                            //     
                            //     //Apply all the effects of a collision with this object
                            //     tuple.child.gameObject.SetActive(!collides);
                            // }
                        }
                        else
                        {
                            tuple.child.gameObject.SetActive(false);
                        }
                    });
            }
        }

        GameManager.Instance.timeScale = baseTimeSpeed / targetRotationSpeed;
        planetMaterial.SetFloat(Shader.PropertyToID(Constants.PLANET_ROTATION_SPEED_REF), targetRotationSpeed);
    }

    private void copyToGameObject(GameObject g, GameObjectProfile gp)
    {
        var rB = g.GetComponent<Rigidbody2D>();
        g.transform.position = gp.Position;
        g.transform.localScale = gp.Scale;
        
        if (rB)
        {
            rB.mass = gp.Mass;
            rB.velocity = gp.Velocity;
        }

        if (g.GetComponent<Planet>() && g.GetComponent<PlanetSurfaceEffects>())
        {
            g.GetComponent<Planet>().extraMass = gp.ExtraMass; 
            g.GetComponent<PlanetSurfaceEffects>().m_GlobalHits = gp.Hits;
            g.GetComponent<PlanetSurfaceEffects>().applyEffects();
            g.GetComponent<Darwin>().restartEvolution(gp.ChemicalProfile);

            //Reset milestones as they were at this timestamp
            if (gp.Milestones != null && gp.Milestones.Count > 0)
            {
                var milestoneChange = new Dictionary<MilestoneType, Milestone>();

                for (int i = 0; i < MilestoneTypeHandler.milestoneTypes.Count; i++)
                {
                    milestoneChange.Add(MilestoneTypeHandler.milestoneTypes[i], new Milestone(MilestoneTypeHandler.milestoneTypes[i], Constants.MILESTONE_DEFAULT_CAP));
                }

                for (int i = 0; i < gp.Milestones.Count; i++)
                {
                    milestoneChange[gp.Milestones[i].Item1] = new Milestone(
                        gp.Milestones[i].Item1,
                        MilestoneTypeHandler.milestoneLevelCap[gp.Milestones[i].Item1],
                        gp.Milestones[i].Item2
                    );
                }
                
                GameManager.Instance.milestones = milestoneChange;
            }
        }

        g.GetComponent<Renderer>().material = gp.Material;
        g.SetActive(gp.IsActive);
    }

    private Vector3 getPositionInTheFuture(GameObjectProfile gp, float time)
    {
        //Legge oraria => Se = v * (Te - Ti) + Si
        var scaledTime = time / GameManager.Instance.timeScale;

        return gp.Position + new Vector3(gp.Velocity.x * scaledTime, gp.Velocity.y * scaledTime, 0); //scale
    }
}