﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class Timeline : MonoBehaviour
{
    private RectTransform _rectTransform;

    public float rotationTime = 2f;
    public float actualRotationTime = 0f;
    private int timeDirection = 1;
    public float elapsedTime = 0f;

    public float slidingStep = -80f; //TODO: Let it proportional to screen size
    
    public float startingX = 0;
    public float targetX = -80f; //TODO: Let it proportional to screen size

    private RectTransform[] _yearsLocations;
    private Camera mainCamera;
    
    // Start is called before the first frame update
    void Start()
    {
        //Setting the initial status
        _rectTransform = GetComponent<RectTransform>();
        startingX = _rectTransform.localPosition.x;
        targetX = startingX + slidingStep;
        actualRotationTime = rotationTime;
        mainCamera = Camera.main;
        
        _yearsLocations = Utils<Transform>.ToIEnumerable(transform.GetEnumerator())
            .Select(t => t.GetComponent<RectTransform>()).ToArray();
        
        //Collecting dates from gamemanager
        GameManager.Instance.years
            .Zip(Utils<Transform>.ToIEnumerable(transform.GetEnumerator()), 
                (l, t) => (l, t.GetComponent<Text>()))
            .ToList()
            .ForEach((tuple =>
            {
                tuple.Item2.text = tuple.l.ToString();
            }));
    }

    // Update is called once per frame
    void Update()
    {
        //Take the time direction
        var oldTimeDirection = timeDirection;
        timeDirection =  (int) Mathf.Sign(GameManager.Instance.timeScale);

        if (oldTimeDirection != timeDirection)
        {
            startingX = _rectTransform.localPosition.x;
            targetX = startingX + slidingStep * timeDirection;
            elapsedTime = 0;
        }

        //Update the period of the rotation
        actualRotationTime = Mathf.Abs(GameManager.Instance.timeScale * rotationTime);
        
        //Rotate
        _rectTransform.localPosition = new Vector3(
            Mathf.Lerp(startingX, targetX, elapsedTime / actualRotationTime), 
            _rectTransform.localPosition.y, 
            0f
            );
        //Check time
        elapsedTime =  Mathf.Min(elapsedTime + Time.deltaTime, actualRotationTime);

        if (Math.Abs(elapsedTime - actualRotationTime) < 0.01f)
        {
            nextRotation();
        }
        
    }

    private void LateUpdate()
    {
        GameManager.Instance.years.Zip(
                _yearsLocations, (l, rectTransform) => (l, rectTransform.GetComponent<Text>()))
            .ToList()
            .ForEach(tuple =>
            {
                tuple.Item2.text = tuple.l.ToString();
            });
    }

    private void nextRotation()
    {
        //Scroll left the band
        startingX = targetX;
        targetX = targetX + slidingStep * timeDirection;
        //Reset timer
        elapsedTime = 0;

        //According to the direction of the rotation check the first or the last element
        var positionToCheck = getRefYear(first: true).position.x;
        var thresholdRelativeToScreenSize = timeDirection > 0 ? -getRefYear().rect.width : mainCamera.pixelRect.width;
        
        //Rotate target element - Update Game manager
        if (positionToCheck - thresholdRelativeToScreenSize <= 0 || timeDirection < 0 && positionToCheck - thresholdRelativeToScreenSize >= 0)
        {
            //Take info to relocate the first or last element
            var yearToRelocate = getRefYear(true);
            var yearRelocationReference = getRefYear();
            
            //Shift left or right the years
            _yearsLocations = Utils<RectTransform>.shift(_yearsLocations, right: timeDirection < 0);
            GameManager.Instance.years = Utils<long>.shift(GameManager.Instance.years, right: timeDirection < 0);
            yearToRelocate.localPosition = yearRelocationReference.localPosition + (Vector3.right * (Mathf.Abs(slidingStep) * timeDirection));
            
            //Update years master
            var yearsListLength = GameManager.Instance.years.Length;
            
            GameManager.Instance.years[timeDirection > 0 ? yearsListLength - 1 : 0] = 
                GameManager.Instance.years[timeDirection > 0 ? yearsListLength - 2 : 1] + GameManager.Instance.timeStep * timeDirection;
            
            //Handle exception when the user try to go backward more than 0.
            if (GameManager.Instance.years.First() == 0)
            {
                GameManager.Instance.timeMachine.StopCoroutine(GameManager.Instance.currentTimeMachineJob);
                GameManager.Instance.timeMachine.StartCoroutine(GameManager.Instance.timeMachine.accelerateFor(0,
                    GameManager.Instance.timeMachine.getBaseRotationSpeed()));
            }
        }
    }

    private RectTransform getRefYear(bool first = false)
    {
        return (!first ? timeDirection > 0 : timeDirection < 0)
            ? _yearsLocations.Last() 
            : _yearsLocations.First();
    }
}
