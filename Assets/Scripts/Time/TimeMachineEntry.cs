﻿using System.Collections.Generic;
using Utilities;

public class TimeMachineEntry
{
    private Dictionary<string, Dictionary<int, GameObjectProfile>> profiles;

    public TimeMachineEntry()
    {
        profiles = new Dictionary<string, Dictionary<int, GameObjectProfile>>();
    }

    public Dictionary<string, Dictionary<int, GameObjectProfile>> Profiles
    {
        get => profiles;
        set => profiles = value;
    }

    public static TimeMachineEntry current()
    {
        var timeMachineEntry = new TimeMachineEntry();

        GameManager.Instance.PoolTags.ForEach(t => timeMachineEntry.Profiles.Add(t, new Dictionary<int, GameObjectProfile>()));
        
        //Create a snapshot of the universe
        // Utils<Transform>.ToIEnumerable(GameManager.Instance.MainPool.GetEnumerator())
        //     .SelectMany(t =>  Utils<Transform>.ToIEnumerable(t.GetEnumerator()), 
        //         (t, child) => (t.tag, Utils.toProfile(child.gameObject)))
        //     .ToList()
        //     .ForEach(tuple => timeMachineEntry.Profiles[tuple.tag].Add(tuple.Item2.ID, tuple.Item2));

        var poolTransform = GameManager.Instance.MainPool;

        for (int i = 0; i < poolTransform.childCount; i++)
        {
            var currentPool = poolTransform.GetChild(i);

            for (int j = 0; j < currentPool.childCount; j++)
            {
                var profile = Utils.toProfile(currentPool.GetChild(j).gameObject);
                timeMachineEntry.Profiles[currentPool.tag].Add(profile.ID, profile);
            }
        }
        
        return timeMachineEntry;
    }

}


