using UnityEngine;

namespace Utilities
{
    public class Utils
    {
        public static bool isMobile()
        {
            return Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer;
        }
    }
}